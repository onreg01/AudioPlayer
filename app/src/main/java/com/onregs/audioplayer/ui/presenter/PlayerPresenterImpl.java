package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicActionImpl;
import com.onregs.audioplayer.ui.view.for_view.BaseView;
import com.onregs.audioplayer.ui.view.PlayerView;

import org.androidannotations.annotations.EBean;

/**
 * Created by vadim on 29.11.2014.
 */

@EBean
public class PlayerPresenterImpl extends MusicActionImpl implements PlayerFragmentPresenter
{

    PlayerView playerView;

    @Override
    public void init(BaseView view)
    {
        super.init(view);
        playerView = (PlayerView) view;
    }

}
