package com.onregs.audioplayer.ui.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;

import com.onregs.audioplayer.Player.Player;
import com.onregs.audioplayer.Player.player_model.PlayerActions;
import com.onregs.audioplayer.Player.player_model.PlayerUtils;
import com.onregs.audioplayer.R;
import com.onregs.audioplayer.ui.interactor.tabs.InitTabsImpl;
import com.onregs.audioplayer.ui.presenter.StartActivityPresenter;
import com.onregs.audioplayer.ui.presenter.StartActivityPresenterImpl;
import com.onregs.audioplayer.ui.view.StartActivityView;
import com.onregs.audioplayer.utils.StatePlayer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;

@OptionsMenu(R.menu.start_activity)
@EActivity(R.layout.activity_start)
public class StartActivity extends Activity implements StartActivityView
{

    @Bean(StartActivityPresenterImpl.class)
    StartActivityPresenter presenter;

    ActionBar actionBar;

    private PlayerActions playerActions;
    private PlayerUtils playerUtils;

    @AfterViews
    public void init()
    {
        presenter.init(this);
        playerActions = Player.getInstance(this);
        playerUtils = Player.getInstance(this);
        actionBar = presenter.initTabs(new InitTabsImpl(getActionBar(), this));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);

        if (savedInstanceState != null)
        {
            actionBar.setSelectedNavigationItem(savedInstanceState.getInt("selectedTab"));
        }
    }

    @Override
    public Activity getContainer()
    {
        return null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt("selectedTab", actionBar.getSelectedTab().getPosition());
    }


    @OptionsItem(R.id.refresh_music)
    public void save()
    {
        if (playerUtils.getPlayer() != null)
        {
            StatePlayer.saveState(this, playerUtils);
        }
        finish();
        System.exit(0);
    }
}
