package com.onregs.audioplayer.ui.interactor.tabs;

import android.app.ActionBar;
import android.content.Context;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.ui.fragments.PlayerFragment;

/**
 * Created by vadim on 21.11.2014.
 */

public class InitTabsImpl implements InitTabs
{
    private ActionBar actionBar;
    private Context context;

    public InitTabsImpl(ActionBar actionBar, Context context)
    {
        this.actionBar = actionBar;
        this.context = context;
    }

    @Override
    public ActionBar init()
    {
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        String[] titleBar = {context.getResources().getString(R.string.Player), context.getResources().getString(R.string.radio)};
        for (String title : titleBar)
        {
            setTab(title);
        }

        return actionBar;
    }


    private void setTab(String title)
    {
        ActionBar.Tab tab = actionBar.newTab();
        tab.setText(title);
        tab.setTabListener(new ActionBar.TabListener()
        {
            @Override
            public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction)
            {

                switch (tab.getPosition())
                {
                    case 0:
                        fragmentTransaction.replace(R.id.frameLayout, PlayerFragment.build(), PlayerFragment.TAG);
                        break;
                    case 1:
//                        fragmentTransaction.replace(R.id.frameLayout, RadioFragment.build(), RadioFragment.TAG);
                        break;
                    default:
                        break;
                }

            }

            @Override
            public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction)
            {

            }

            @Override
            public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction)
            {

            }
        });

        actionBar.addTab(tab);
    }


}
