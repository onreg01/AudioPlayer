package com.onregs.audioplayer.ui.presenter.for_presenter;

import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.interactor.resume_view.RestoreSelectedItemColorListener;

/**
 * Created by vadim on 11.12.2014.
 */
public interface RestoreSelectedItemColorPresenter extends RestoreSelectedItemColorListener
{
    void restoreSelectedItemColor(OnResumeAction onResumeAction, PlayList playList);
}
