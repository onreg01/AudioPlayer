package com.onregs.audioplayer.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.utils.PlayListUtils;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

/**
 * Created by vadim on 23.12.2014.
 */

@EFragment
public class DialogLongClickPlayLists extends DialogFragment
{

    public static final String TAG = DialogLongClickPlayLists.class.getSimpleName();

    public static DialogLongClickPlayLists build(long id, String name)
    {
        return DialogLongClickPlayLists_.builder().playListId(id).playListName(name).build();
    }

    private static final int DELETE = 0;
    private static final int RENAME = 1;

    @FragmentArg
    long playListId;

    @FragmentArg
    String playListName;

    String[] items;

    DialogLongClickPlayListsInterface dialogLongClickPlayListsInterface;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        dialogLongClickPlayListsInterface = (DialogLongClickPlayListsInterface) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        items = getResources().getStringArray(R.array.long_click_items);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_DARK);
        builder.setTitle(playListName);
        builder.setItems(items, listener);
        AlertDialog dialog = builder.create();

        return dialog;
    }

    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
            switch (i)
            {
                case DELETE:
                    deletePlayList();
                    break;
                case RENAME:
                    renamePlayList();
                    break;
                default:
                    break;
            }
        }
    };

    public void deletePlayList()
    {
        PlayListUtils.deletePlaylist(dialogLongClickPlayListsInterface.getResolver(), playListId);
        dialogLongClickPlayListsInterface.showMessage(playListName + " " + getResources().getString(R.string.playlist_deleted));
        dialogLongClickPlayListsInterface.updateList();
    }

    public void renamePlayList()
    {
        DialogFragment dialog = DialogNewName.build(true, playListId, playListName);
        dialog.show(getFragmentManager(), DialogNewName.TAG);
    }


    public interface DialogLongClickPlayListsInterface extends BaseDialogInterface
    {

    }
}
