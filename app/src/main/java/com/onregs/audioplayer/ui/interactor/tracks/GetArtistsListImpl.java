package com.onregs.audioplayer.ui.interactor.tracks;

import android.content.Context;

import com.onregs.audioplayer.data.Album;
import com.onregs.audioplayer.data.Artist;

import java.util.ArrayList;

/**
 * Created by vadim on 14.12.2014.
 */
public class GetArtistsListImpl implements GetArtistsList
{
    private Context context;
    private ArrayList<Artist> artists;
    private GetAlbumsWithTracks getAlbumsWithTracks;

    public GetArtistsListImpl(Context context)
    {
        this.context = context;
        artists = new ArrayList<Artist>();
        getAlbumsWithTracks = new GetAlbumsWithTracksImpl(context);
    }

    @Override
    public ArrayList<Artist> getArtists()
    {
        ArrayList<Album> albums = getAlbumsWithTracks.getAlbums();

        for (Album album : albums)
        {
            Artist newArtist = new Artist();
            newArtist.setArtistId(album.getArtistInfo().getArtistId());
            newArtist.setArtistName(album.getArtistInfo().getArtistName());
            if (!artists.contains(newArtist))
            {
                newArtist.addAlbum(album);
                artists.add(newArtist);
            }
            else
            {
                for (Artist artist : artists)
                {
                    if (artist.equals(newArtist))
                    {
                        artist.addAlbum(album);
                    }
                }
            }
        }

        return artists;
    }
}
