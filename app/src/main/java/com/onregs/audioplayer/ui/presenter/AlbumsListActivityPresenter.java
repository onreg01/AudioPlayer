package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.interactor.tracks.GetAlbumsWithTracks;
import com.onregs.audioplayer.ui.presenter.for_presenter.BasePresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicAction;

/**
 * Created by vadim on 09.12.2014.
 */
public interface AlbumsListActivityPresenter extends BasePresenter, MusicAction
{
    void setListViewAlbums(GetAlbumsWithTracks getAlbumsWithTracks);
}
