package com.onregs.audioplayer.ui.dialogs.old_dialogs;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.ui.dialogs.BaseDialogInterface;
import com.onregs.audioplayer.utils.PlayListUtils;
import com.onregs.audioplayer.utils.edit_text.CustomTextWatcher;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vadim on 20.12.2014.
 */
@EFragment(R.layout.dialog_newplaylist)
public class DialogNewPlayList extends DialogFragment
{
    public static final String TAG = DialogNewPlayList.class.getSimpleName();

//    public static DialogNewPlayList build()
//    {
//        return DialogNewPlayList_.builder().build();
//    }
//
//    public static DialogNewPlayList build(boolean isRename, long id, String oldName)
//    {
//        return DialogNewPlayList_.builder().isRename(isRename).playListId(id).playListName(oldName).build();
//    }

    @ViewById(R.id.tvEditText)
    EditText editText;

    @ViewById(R.id.buttonSave)
    Button buttonSave;

    @FragmentArg
    boolean isRename;

    @FragmentArg
    long playListId;

    @FragmentArg
    String playListName;

    DialogNewPlayListInterFace dialogNewPlayListInterFace;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        dialogNewPlayListInterFace = (DialogNewPlayListInterFace) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        getDialog().getWindow().setTitle(getResources().getString(R.string.new_playlist_title));
        getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_color_dialogtitle));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @AfterViews
    public void setButtonState()
    {
        buttonSave.setClickable(false);
    }

    @AfterViews
    public void setEditTextWatcher()
    {
        editText.addTextChangedListener(new CustomTextWatcher()
        {
            @Override
            public void afterTextChanged(Editable editable)
            {
                if (TextUtils.isEmpty(editable))
                {
                    switchButtonState(false, getResources().getColor(R.color.back));
                }
                else
                {
                    switchButtonState(true, getResources().getColor(R.color.white));
                }
            }
        });
    }

    @Click
    public void buttonSave()
    {
        String text = editText.getText().toString();
        if (isRename)
        {
            renamePlayList(text);
        }

        else

        {
            createNewPlayList(text);
        }
    }

    private void renamePlayList(String text)
    {
        long state = PlayListUtils.renamePlaylist(dialogNewPlayListInterFace.getResolver(), playListId, text);
        if (state != 0)
        {
            sendMessage(text, getResources().getString(R.string.playlist_renamed));
            closeDialogAndUpdateList();
        }
        else
        {
            sendMessage(text, getResources().getString(R.string.playlist_name_already));
        }
    }

    private void createNewPlayList(String text)
    {
        long state = PlayListUtils.createPlaylist(dialogNewPlayListInterFace.getResolver(), text);

        if (state != 0)
        {
            sendMessage(text, getResources().getString(R.string.playlist_added));
//            showAddSongDialog();
            closeDialogAndUpdateList();
        }
        else
        {
            sendMessage(text, getResources().getString(R.string.playlist_name_already));
        }
    }

    @Click
    public void buttonCancel()
    {
        dismiss();
    }

    private void switchButtonState(Boolean stateClickable, int color)
    {
        buttonSave.setClickable(stateClickable);
        buttonSave.setTextColor(color);
    }


    public void sendMessage(String text, String message)
    {
        dialogNewPlayListInterFace.showMessage(text + " " + message);
    }

    private void closeDialogAndUpdateList()
    {
        dialogNewPlayListInterFace.updateList();
        dismiss();
    }


//    private void showAddSongDialog()
//    {
//        DialogFragment dialog = DialogAddSongs.build();
//        dialog.show(getFragmentManager(), DialogAddSongs.TAG);
//    }

    public interface DialogNewPlayListInterFace extends BaseDialogInterface
    {

    }
}
