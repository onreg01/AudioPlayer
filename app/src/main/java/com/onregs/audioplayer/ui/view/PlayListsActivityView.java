package com.onregs.audioplayer.ui.view;

import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.view.for_view.ActionView;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

import java.util.ArrayList;

/**
 * Created by vadim on 17.12.2014.
 */
public interface PlayListsActivityView extends BaseView, ActionView
{
    void showPlayLists(ArrayList<PlayList> playLists);
}
