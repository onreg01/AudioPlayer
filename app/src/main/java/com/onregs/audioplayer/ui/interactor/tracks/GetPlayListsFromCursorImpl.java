package com.onregs.audioplayer.ui.interactor.tracks;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.utils.PlayListUtils;

import java.util.ArrayList;

/**
 * Created by vadim on 20.12.2014.
 */
public class GetPlayListsFromCursorImpl implements GetPlayListsFromCursor
{
    private Context context;
    private ArrayList<PlayList> playLists;

    public GetPlayListsFromCursorImpl(Context context)
    {
        this.context = context;
        playLists = new ArrayList<PlayList>();
    }

    @Override
    public ArrayList<PlayList> getPlayLists()
    {
        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = PlayListUtils.getAllPlayLists(resolver);

        if (cursor.moveToFirst())
        {
            do
            {
                int playListIdColumn = cursor.getColumnIndex(MediaStore.Audio.Playlists._ID);
                int playListNameColumn = cursor.getColumnIndex(MediaStore.Audio.Playlists.NAME);

                PlayList playList = new PlayList(cursor.getString(playListNameColumn));
                playList.setPlayListId(cursor.getLong(playListIdColumn));

                fillPlayList(playList, resolver);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return playLists;
    }

    private void fillPlayList(PlayList playList, ContentResolver resolver)
    {
        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playList.getPlayListId());
        Cursor cursor = resolver.query(uri, null, null, null, null);

        GetAllTracks getAllTracks = new GetAllTracksImpl();
        playList.setTracks(getAllTracks.getAllTracks(context, cursor));

        playLists.add(playList);
    }
}
