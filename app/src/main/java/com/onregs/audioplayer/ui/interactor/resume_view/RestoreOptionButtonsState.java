package com.onregs.audioplayer.ui.interactor.resume_view;

import android.graphics.drawable.Drawable;

/**
 * Created by vadim on 31.07.2015.
 */
public interface RestoreOptionButtonsState
{
    void switchImageShuffle(Drawable drawable);

    void switchImageRepeat(Drawable drawable);
}
