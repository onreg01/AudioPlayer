package com.onregs.audioplayer.ui.interactor.tracks;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.onregs.audioplayer.data.Track;
import com.onregs.audioplayer.data.TrackDuration;
import com.onregs.audioplayer.data.dataSorting.SortingByTrackName;
import com.onregs.audioplayer.data.track_info.AlbumInfo;
import com.onregs.audioplayer.data.track_info.ArtistInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by vadim on 29.11.2014.
 */

public class GetAllTracksImpl implements GetAllTracks
{
    private ArrayList<Track> tracks = new ArrayList<Track>();
    private Track track;

    @Override
    public ArrayList<Track> getAllTracks(Context context, Cursor cursor)
    {

        if (cursor == null)
        {
            cursor = getCursorWithMusic(context);
        }
        if (cursor.moveToFirst())
        {
            fillMusicList(cursor, context);
        }
        startSorting(tracks, new SortingByTrackName());
        cursor.close();
        return tracks;
    }

    private Cursor getCursorWithMusic(Context context)
    {
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        return contentResolver.query(uri, null, null, null, null);
    }

    private void fillMusicList(Cursor cursor, Context context)
    {
        int trackIdColumn = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
        int trackNameColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
        int pathColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);

        int trackDurationColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION);

        int albumIdColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
        int trackAlbumNameColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);

        int artistIdColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID);
        int artistNameColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);


        do
        {
            if (checkDuration(cursor.getLong(trackDurationColumn)))
            {
                String path = cursor.getString(pathColumn);
                if (!filter(path))
                {
                    continue;
                }
                track = new Track(
                        cursor.getInt(trackIdColumn),
                        cursor.getString(trackNameColumn),
                        path,
                        new TrackDuration(cursor.getLong(trackDurationColumn)),
                        new AlbumInfo(cursor.getInt(albumIdColumn), cursor.getString(trackAlbumNameColumn)),
                        new ArtistInfo(cursor.getInt(artistIdColumn), cursor.getString(artistNameColumn)));
                tracks.add(track);

            }

        } while (cursor.moveToNext());
    }


    private boolean checkDuration(Long duration)
    {
        if (duration > 1)
        {
            return true;
        }
        return false;
    }

    public boolean filter(String path)
    {
        return (path.endsWith(".mp3") || path.endsWith(".MP3"));
    }

    private <T extends Comparator<Track>> ArrayList<Track> startSorting(ArrayList<Track> tracks, T sorting)
    {
        Collections.sort(tracks, sorting);

        return tracks;
    }
}
