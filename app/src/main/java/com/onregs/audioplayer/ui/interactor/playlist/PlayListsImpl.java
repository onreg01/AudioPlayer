package com.onregs.audioplayer.ui.interactor.playlist;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;

/**
 * Created by vadim on 17.12.2014.
 */
public class PlayListsImpl implements PlayLists
{
    private Context context;
    public PlayListsImpl(Context context)
    {
        this.context = context;
    }

    @Override
    public Cursor getPlayLists()
    {
//        ContentValues mInserts = new ContentValues();
//        mInserts.put(MediaStore.Audio.Playlists.NAME, "Onreg");
//        mInserts.put(MediaStore.Audio.Playlists.DATE_ADDED, System.currentTimeMillis());
//        mInserts.put(MediaStore.Audio.Playlists.DATE_MODIFIED, System.currentTimeMillis());
//
//        ContentResolver contentResolver = context.getContentResolver();
//        Uri uri =  contentResolver.insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, mInserts);
//        long playlistId = Long.parseLong(uri.getLastPathSegment());
//
//
//        addSongs(playlistId);

        ContentResolver contentResolver = context.getContentResolver();
        return contentResolver.query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,null,null,null,null);
    }
//
//    private void addSongs(long playlistId)
//    {
//        ContentResolver contentResolver = context.getContentResolver();
//        String[] cols = new String[] {
//                "count(*)"
//        };
//        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
//        Cursor cur = contentResolver.query(uri, cols, null, null, null);
//        cur.moveToFirst();
//        final int base = cur.getInt(0);
//        cur.close();
//
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, Integer.valueOf(base + 6804));
//        values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, 6804);
//        Uri track = contentResolver.insert(uri, values);
//
//        getTracK(playlistId);
//    }
//
//    private void getTracK(long playlistId)
//    {
//        ContentResolver contentResolver = context.getContentResolver();
//        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
//
//        Cursor cur = contentResolver.query(uri, null, null, null, null);
//        cur.moveToFirst();
//
//        String title = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE));
//        String a = "asd";
//    }
}
