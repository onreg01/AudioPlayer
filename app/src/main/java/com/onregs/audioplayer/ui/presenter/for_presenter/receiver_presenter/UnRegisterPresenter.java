package com.onregs.audioplayer.ui.presenter.for_presenter.receiver_presenter;

import android.content.Context;

/**
 * Created by vadim on 06.12.2014.
 */
public interface UnRegisterPresenter
{
    public void unregisterReceiver(Context context);
}
