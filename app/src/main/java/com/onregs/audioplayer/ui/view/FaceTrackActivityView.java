package com.onregs.audioplayer.ui.view;

import com.onregs.audioplayer.ui.view.for_view.BaseView;
import com.onregs.audioplayer.ui.view.for_view.FaceTrackActivityActionView;

/**
 * Created by vadim on 29.07.2015.
 */
public interface FaceTrackActivityView extends BaseView, FaceTrackActivityActionView
{

}
