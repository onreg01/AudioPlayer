package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.presenter.for_presenter.BasePresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicAction;

/**
 * Created by vadim on 29.11.2014.
 */
public interface PlayerFragmentPresenter extends BasePresenter, MusicAction
{
}
