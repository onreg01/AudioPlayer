package com.onregs.audioplayer.ui.dialogs;

import android.content.ContentResolver;

/**
 * Created by vadim on 22.12.2014.
 */
public interface BaseDialogInterface
{
    ContentResolver getResolver();

    void showMessage(String message);

    void updateList();
}
