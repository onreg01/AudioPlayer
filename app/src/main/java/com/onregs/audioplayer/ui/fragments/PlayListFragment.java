package com.onregs.audioplayer.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.onregs.audioplayer.Player.Player;
import com.onregs.audioplayer.Player.player_model.PlayerActions;
import com.onregs.audioplayer.Player.player_model.PlayerUtils;
import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.adapters.AllMusicListAdapter;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeActionIml;
import com.onregs.audioplayer.ui.presenter.PlayListFragmentPresenter;
import com.onregs.audioplayer.ui.presenter.PlayListFragmentPresenterImpl;
import com.onregs.audioplayer.ui.view.PlayListFragmentView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_play_list)
public class PlayListFragment extends Fragment implements Player.ChangeListViewItemColor, PlayListFragmentView
{
    public static final String TAG = PlayerFragment.class.getSimpleName();

    public static PlayListFragment build()
    {
        return PlayListFragment_.builder().build();
    }

    @Bean(PlayListFragmentPresenterImpl.class)
    PlayListFragmentPresenter presenter;

    private OnResumeAction onResumeAction;

    @ViewById(R.id.lvPlayList)
    ListView listView;

    private PlayList playList;

    private PlayListFragmentInterface playListFragmentInterface;

    private PlayerActions playerActions;
    private PlayerUtils playerUtils;


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        playerUtils = Player.getInstance(activity);
        playerActions = Player.getInstance(activity);
        onResumeAction = new OnResumeActionIml(activity);

        playListFragmentInterface = (PlayListFragmentInterface) activity;

    }

    @AfterViews
    public void init()
    {
        presenter.init(this);
        playerUtils.initView(this);

    }

    @AfterViews
    public void initListView()
    {
        playList = playListFragmentInterface.getPlayList();
        AllMusicListAdapter allMusicListAdapter = new AllMusicListAdapter(getActivity(), R.layout.all_music_item, playList.getTracks());
        listView.setAdapter(allMusicListAdapter);
        setListener();
    }

    private void setListener()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                playerUtils.initPlaylist(playList);
                playerUtils.stopTimer();
                playerActions.start(position);
                playListFragmentInterface.changeImage(getResources().getDrawable(R.drawable.ic_action_pause));
            }
        });
    }


    @Override
    public Activity getContainer()
    {
        return getActivity();
    }

    @Override
    public void restoreSelectedItemColor(int position)
    {
        listView.setItemChecked(position, true);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.restoreSelectedItemColor(onResumeAction, playList);
    }

    @Override
    public void switchColor()
    {
        presenter.restoreSelectedItemColor(onResumeAction, playList);
    }


    public interface PlayListFragmentInterface
    {
        PlayList getPlayList();

        void changeImage(Drawable drawable);
    }
}
