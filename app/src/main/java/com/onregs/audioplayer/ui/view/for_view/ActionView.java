package com.onregs.audioplayer.ui.view.for_view;

/**
 * Created by vadim on 09.12.2014.
 */
public interface ActionView extends ChangeImage
{
    void setProgressBar(long max);

    void updateProgress(long milliseconds);

    void setTrack (String trackName, String trackAuthor);


}
