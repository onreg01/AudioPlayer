package com.onregs.audioplayer.ui.view;

import com.onregs.audioplayer.ui.view.for_view.ActionView;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

/**
 * Created by vadim on 29.11.2014.
 */
public interface PlayerView extends BaseView, ActionView
{

}
