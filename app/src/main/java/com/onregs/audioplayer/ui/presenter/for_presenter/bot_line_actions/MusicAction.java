package com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions;

import com.onregs.audioplayer.ui.interactor.broadcast_receiver.ReceiverActionListener;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickActionListener;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeActionListener;
import com.onregs.audioplayer.ui.presenter.for_presenter.ButtonActionPresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.OnResumePresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.receiver_presenter.ReceiverPresenter;

/**
 * Created by vadim on 09.12.2014.
 */
public interface MusicAction extends ReceiverPresenter,
        ButtonActionPresenter, ReceiverActionListener, ButtonClickActionListener, OnResumePresenter, OnResumeActionListener
{

}
