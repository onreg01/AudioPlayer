package com.onregs.audioplayer.ui.presenter;

import android.app.ActionBar;

import com.onregs.audioplayer.ui.interactor.tabs.InitTabs;
import com.onregs.audioplayer.ui.presenter.for_presenter.BasePresenter;

/**
 * Created by vadim on 20.11.2014.
 */
public interface StartActivityPresenter extends BasePresenter
{
    ActionBar initTabs(InitTabs initTabs);
}
