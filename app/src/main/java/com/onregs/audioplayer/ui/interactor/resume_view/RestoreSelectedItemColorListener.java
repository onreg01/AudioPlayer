package com.onregs.audioplayer.ui.interactor.resume_view;

/**
 * Created by vadim on 11.12.2014.
 */
public interface RestoreSelectedItemColorListener
{
    void restoreSelectedItemColor(int position);
}
