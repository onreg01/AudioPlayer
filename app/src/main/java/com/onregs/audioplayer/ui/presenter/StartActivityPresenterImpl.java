package com.onregs.audioplayer.ui.presenter;

import android.app.ActionBar;

import com.onregs.audioplayer.ui.interactor.tabs.InitTabs;
import com.onregs.audioplayer.ui.view.for_view.BaseView;
import com.onregs.audioplayer.ui.view.StartActivityView;

import org.androidannotations.annotations.EBean;


/**
 * Created by vadim on 20.11.2014.
 */

@EBean
public class StartActivityPresenterImpl implements StartActivityPresenter
{

    StartActivityView startActivityView;

    @Override
    public void init(BaseView view)
    {
        startActivityView = (StartActivityView) view;
    }

    @Override
    public ActionBar initTabs(InitTabs initTabs)
    {
        return  initTabs.init();
    }

}
