package com.onregs.audioplayer.ui.interactor.resume_view;

import android.content.Context;

import com.onregs.audioplayer.Player.Player;
import com.onregs.audioplayer.Player.player_model.PlayerActions;
import com.onregs.audioplayer.Player.player_model.PlayerUtils;
import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.data.Track;

/**
 * Created by vadim on 06.12.2014.
 */
public class OnResumeActionIml implements OnResumeAction
{
    private Context context;
    private PlayerActions playerActions;
    private PlayerUtils playerUtils;

    public OnResumeActionIml(Context context)
    {
        this.context = context;
        playerActions = Player.getInstance(context);
        playerUtils = Player.getInstance(context);
    }

    @Override
    public void onResume(OnResumeActionListener onResumeActionListener)
    {
        if ((checkPlayer() || checkStateAfterExit()) && checkPlayList())
        {
            Track track = playerUtils.getCurrentTrack();
            onResumeActionListener.prepareSong(((int) playerUtils.getCurrentTrack().getDuration().getLongDuration()),
                    track.getTrackName(),track.getArtistInfo().getArtistName());
            if (checkPlaying())
            {
                onResumeActionListener.switchImagePlay(context.getResources().getDrawable(R.drawable.ic_action_pause));
            }
            else
            {
                onResumeActionListener.switchImagePlay(context.getResources().getDrawable(R.drawable.ic_action_play));
            }
        }
        onResumeActionListener.registerReceiver(context);
    }

    @Override
    public void restoreSelectedItemColor(RestoreSelectedItemColorListener restoreSelectedItemColor, PlayList playList)
    {
        if (checkPlayer() || checkStateAfterExit() && checkPlayList())
        {
            int trackId = playerUtils.getCurrentTrack().getTrackId();

            for (Track song : playList.getTracks())
            {
                if (song.getTrackId() == trackId)
                {
                    restoreSelectedItemColor.restoreSelectedItemColor(song.getTrackPosition());
                }
            }
        }
    }

    @Override
    public void restoreBtnShuffle(RestoreOptionButtonsState listener)
    {
        boolean state = playerUtils.isShuffle();

        if(state == true)
        {
            listener.switchImageShuffle(context.getResources().getDrawable(R.drawable.ic_action_shuffle));
        }
        else
        {
            listener.switchImageShuffle(context.getResources().getDrawable(R.drawable.ic_action_do_not_shuffle));
        }
    }

    @Override
    public void restoreBtnRepeat(RestoreOptionButtonsState listener)
    {

        boolean state = playerUtils.isRepeat();

        if(state == true)
        {
            listener.switchImageRepeat(context.getResources().getDrawable(R.drawable.ic_action_repeat));
        }
        else
        {
            listener.switchImageRepeat(context.getResources().getDrawable(R.drawable.ic_action_repeat_nothing));
        }
    }

    private boolean checkPlayer()
    {
        return playerUtils.getPlayer() != null;
    }

    private boolean checkPlaying()
    {
        if (checkPlayer())
        {
            return playerUtils.getPlayer().isPlaying();
        }
        return false;
    }

    private boolean checkStateAfterExit()
    {
        return playerUtils.getState();
    }

    private boolean checkPlayList()
    {
        return playerUtils.isPlayListNoEmpty();
    }
}

