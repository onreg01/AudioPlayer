package com.onregs.audioplayer.ui.interactor.broadcast_receiver;

import android.content.Context;

/**
 * Created by vadim on 05.12.2014.
 */
public interface ProgressReceiver
{
    void initReceiver(Context context);

    void removeReceiver(Context context);
}
