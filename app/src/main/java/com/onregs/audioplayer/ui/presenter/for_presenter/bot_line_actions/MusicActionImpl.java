package com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.onregs.audioplayer.ui.activity.FaceTrackActivity;
import com.onregs.audioplayer.ui.interactor.broadcast_receiver.ProgressReceiver;
import com.onregs.audioplayer.ui.interactor.broadcast_receiver.ProgressReceiverIml;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickAction;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.view.FaceTrackActivityView;
import com.onregs.audioplayer.ui.view.for_view.ActionView;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

/**
 * Created by vadim on 09.12.2014.
 */
public class MusicActionImpl implements MusicAction
{
    ActionView actionView;
    FaceTrackActivityView faceTrackActivityView;
    ProgressReceiver receiver;

    public void init(BaseView view)
    {
        this.actionView = (ActionView) view;

        if(view instanceof FaceTrackActivity)
        {
            this.faceTrackActivityView = (FaceTrackActivity) view;
        }
    }

    @Override
    public void onResume(OnResumeAction onResumeAction)
    {
        onResumeAction.onResume(this);
    }

    @Override
    public void registerReceiver(Context context)
    {
        receiver = new ProgressReceiverIml(this);
        receiver.initReceiver(context);
    }

    @Override
    public void unregisterReceiver(Context context)
    {
        receiver.removeReceiver(context);
    }

    @Override
    public void prepareSong(long max, String trackName, String trackAuthor)
    {
        actionView.setProgressBar(max);
        actionView.setTrack(trackName, trackAuthor);
    }

    @Override
    public void updateProgress(long milliseconds)
    {
        actionView.updateProgress(milliseconds);
    }

    @Override
    public void btnPreviousTrack(ButtonClickAction buttonClickAction)
    {
        buttonClickAction.previousTrack(this);
    }

    @Override
    public void btnNextTrack(ButtonClickAction buttonClickAction)
    {
        buttonClickAction.nextTrack(this);
    }

    @Override
    public void btnPlay(ButtonClickAction buttonClickAction)
    {
        buttonClickAction.play(this);
    }

    @Override
    public void btnShuffle(ButtonClickAction buttonClickAction)
    {
        buttonClickAction.shuffle(this);
    }

    @Override
    public void btnRepeat(ButtonClickAction buttonClickAction)
    {
        buttonClickAction.repeat(this);
    }

    @Override
    public void switchImagePlay(Drawable drawable)
    {
        actionView.switchImage(drawable);
    }

    @Override
    public void switchImageShuffle(Drawable drawable)
    {
        faceTrackActivityView.updateBtnShuffle(drawable);
    }

    @Override
    public void switchImageRepeat(Drawable drawable)
    {
        faceTrackActivityView.updateBtnRepeat(drawable);
    }
}
