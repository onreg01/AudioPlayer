package com.onregs.audioplayer.ui.view;

import android.widget.ArrayAdapter;

import com.onregs.audioplayer.ui.view.for_view.BaseView;

/**
 * Created by vadim on 23.07.2015.
 */
public interface MusicToPlayListView extends BaseView
{
    void initListView(ArrayAdapter adapter);
}
