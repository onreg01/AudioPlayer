package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.interactor.resume_view.RestoreOptionButtonsState;
import com.onregs.audioplayer.ui.presenter.for_presenter.BasePresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicAction;

/**
 * Created by vadim on 29.07.2015.
 */
public interface FaceTrackActivityPresenter extends BasePresenter, MusicAction, RestoreOptionButtonsState
{
   void  restoreOptionButton(OnResumeAction onResumeAction);
}
