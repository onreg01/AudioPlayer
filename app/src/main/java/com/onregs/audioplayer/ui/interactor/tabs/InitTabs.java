package com.onregs.audioplayer.ui.interactor.tabs;

import android.app.ActionBar;

/**
 * Created by vadim on 21.11.2014.
 */
public interface InitTabs
{
    ActionBar init();
}
