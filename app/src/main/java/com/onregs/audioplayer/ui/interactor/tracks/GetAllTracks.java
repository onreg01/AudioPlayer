package com.onregs.audioplayer.ui.interactor.tracks;

import android.content.Context;
import android.database.Cursor;

import com.onregs.audioplayer.data.Track;

import java.util.ArrayList;

/**
 * Created by vadim on 29.11.2014.
 */
public interface GetAllTracks
{
    public ArrayList<Track> getAllTracks(Context context, Cursor cursor);
}
