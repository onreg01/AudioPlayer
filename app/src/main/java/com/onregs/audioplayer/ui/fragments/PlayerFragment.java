package com.onregs.audioplayer.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onregs.audioplayer.Player.Player;
import com.onregs.audioplayer.Player.player_model.PlayerActions;
import com.onregs.audioplayer.Player.player_model.PlayerUtils;
import com.onregs.audioplayer.R;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickAction;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickActionImpl;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeActionIml;
import com.onregs.audioplayer.ui.presenter.PlayerFragmentPresenter;
import com.onregs.audioplayer.ui.presenter.PlayerPresenterImpl;
import com.onregs.audioplayer.ui.view.PlayerView;
import com.onregs.audioplayer.utils.Luncher;
import com.onregs.audioplayer.utils.StatePlayer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_player)
public class PlayerFragment extends Fragment implements PlayerView
{
    public static final String TAG = PlayerFragment.class.getSimpleName();

    @Bean(PlayerPresenterImpl.class)
    PlayerFragmentPresenter presenter;

    @ViewById(R.id.all_music)
    TextView allMusic;

    @ViewById
    ProgressBar progressBar;

    @ViewById(R.id.play)
    ImageView playImage;

    @ViewById(R.id.tvTrackName)
    TextView tvTrackName;

    private boolean playListSize;

    private PlayerUtils playerUtils;
    private PlayerActions playerActions;

    private ButtonClickAction buttonClickAction;
    private OnResumeAction onResumeAction;

    public static PlayerFragment build()
    {
        return PlayerFragment_.builder().build();
    }

    @Override
    public void setProgressBar(long max)
    {
        progressBar.setMax((int) max);
    }

    @Override
    public void updateProgress(long milliseconds)
    {
        progressBar.setProgress((int) milliseconds);
    }

    @Override
    public void switchImage(Drawable drawable)
    {
        playImage.setImageDrawable(drawable);
    }

    @Override
    public void setTrack (String trackName, String trackAuthor)
    {
        tvTrackName.setText(trackName);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        buttonClickAction = new ButtonClickActionImpl(activity);
        onResumeAction = new OnResumeActionIml(activity);
        initPlayer(activity);
        StatePlayer.restoreOptionButtons(getContainer(), playerActions);
    }

    public void initPlayer(Activity activity)
    {
        playerUtils = Player.getInstance(activity);
        playerActions = Player.getInstance(activity);
        playerUtils.initPlayerAfterExit();
    }

    @Click(R.id.bot_panel)
    public void showFaceTrack()
    {
        Luncher.startFaceTrackActivity(getContainer());
    }

    @AfterViews
    public void init()
    {
        presenter.init(this);
    }

    @Override
    public Activity getContainer()
    {
        return getActivity();
    }

    @Click(R.id.all_music)
    public void startAllMusicListActivity()
    {
        Luncher.startAllMusicListActivity(getContainer());
    }

    @Click(R.id.albums)
    public void startAlbumsListActivity()
    {
        Luncher.startAlbumsListActivity(getContainer());
    }

    @Click(R.id.artist)
    public void startArtistsListActivity()
    {
        Luncher.startArtistsListActivity(getContainer());
    }

    @Click(R.id.playlist)
    public void startPlayListsActivity()
    {
        Luncher.startPlayListsActivity(getContainer());
    }

    @Click
    public void previousTrack()
    {
        presenter.btnPreviousTrack(buttonClickAction);
    }

    @Click
    public void nextTrack()
    {
        presenter.btnNextTrack(buttonClickAction);
    }

    @Click
    public void play()
    {
        presenter.btnPlay(buttonClickAction);
    }


    @Override
    public void onResume()
    {
        super.onResume();
        presenter.onResume(onResumeAction);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        presenter.unregisterReceiver(getContainer());
    }

}
