package com.onregs.audioplayer.ui.activity;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onregs.audioplayer.Player.Player;
import com.onregs.audioplayer.Player.player_model.PlayerActions;
import com.onregs.audioplayer.Player.player_model.PlayerUtils;
import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.adapters.AllMusicListAdapter;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickAction;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickActionImpl;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeActionIml;
import com.onregs.audioplayer.ui.interactor.tracks.GetAllTracks;
import com.onregs.audioplayer.ui.interactor.tracks.GetAllTracksImpl;
import com.onregs.audioplayer.ui.presenter.AllMusicListActivityPresenter;
import com.onregs.audioplayer.ui.presenter.AllMusicListActivityPresenterImpl;
import com.onregs.audioplayer.ui.view.AllMusicListActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_all_music_list)
public class AllMusicListActivity extends Activity implements AllMusicListActivityView, Player.ChangeListViewItemColor
{

    @Bean(AllMusicListActivityPresenterImpl.class)
    AllMusicListActivityPresenter presenter;

    @ViewById(R.id.lvAllMusic)
    ListView lvAllMusic;

    @ViewById(R.id.progressBar)
    ProgressBar progressBar;

    @ViewById(R.id.play)
    ImageView playImage;

    @ViewById(R.id.tvTrackName)
    TextView tvTrackName;

    private PlayerActions playerActions;
    private PlayerUtils playerUtils;

    private ButtonClickAction buttonClickAction = new ButtonClickActionImpl(this);
    private OnResumeAction onResumeAction = new OnResumeActionIml(this);

    private PlayList playList = new PlayList("All Music");

    @AfterViews
    public void init()
    {
        presenter.init(this);
        playerActions = Player.getInstance(this);
        playerUtils = Player.getInstance(this);
        playerUtils.initView(this);
        initListView();
    }

    public void initListView()
    {
        playList.setTracks(((GetAllTracks) new GetAllTracksImpl()).getAllTracks(this, null));
        if (playList.getTracks() != null)
        {
            AllMusicListAdapter adapter = new AllMusicListAdapter(this, R.layout.all_music_item, playList.getTracks());
            lvAllMusic.setAdapter(adapter);
            setListener();
        }
    }

    private void setListener()
    {
        lvAllMusic.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
            {
                playerUtils.initPlaylist(playList);
                playerUtils.stopTimer();
                playerActions.start(position);
                switchImage(getResources().getDrawable(R.drawable.ic_action_pause));
            }
        });
    }

    @Override
    public void setProgressBar(long max)
    {
        progressBar.setMax((int) max);
    }

    @Override
    public void updateProgress(long milliseconds)
    {
        progressBar.setProgress((int) milliseconds);
    }

    @Override
    public Activity getContainer()
    {
        return this;
    }

    @Click
    public void previousTrack()
    {
        presenter.btnPreviousTrack(buttonClickAction);
    }

    @Click
    public void nextTrack()
    {
        presenter.btnNextTrack(buttonClickAction);
    }

    @Override
    public void switchImage(Drawable drawable)
    {
        playImage.setImageDrawable(drawable);
    }

    @Override
    public void setTrack (String trackName, String trackAuthor)
    {
        tvTrackName.setText(trackName);
    }

    @Click
    public void play()
    {
        presenter.btnPlay(buttonClickAction);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.onResume(onResumeAction);
        presenter.restoreSelectedItemColor(onResumeAction, playList);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        presenter.unregisterReceiver(this);
    }

    @Override
    public void switchColor()
    {
        presenter.restoreSelectedItemColor(onResumeAction, playList);
    }

    @Override
    public void restoreSelectedItemColor(int position)
    {
        lvAllMusic.setItemChecked(position, true);
    }
}
