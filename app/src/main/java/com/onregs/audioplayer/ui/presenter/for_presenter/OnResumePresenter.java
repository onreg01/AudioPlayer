package com.onregs.audioplayer.ui.presenter.for_presenter;

import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;

/**
 * Created by vadim on 06.12.2014.
 */
public interface OnResumePresenter
{
    void onResume(OnResumeAction onResumeAction);
}
