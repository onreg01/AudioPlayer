package com.onregs.audioplayer.ui.fragments;

import android.app.Fragment;

import com.onregs.audioplayer.R;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vadim on 21.11.2014.
 */
@EFragment(R.layout.fragment_radio)
public class RadioFragment extends Fragment
{
    public static final String TAG = RadioFragment.class.getSimpleName();
    public static RadioFragment build()
    {
        return RadioFragment_.builder().build();
    }
}
