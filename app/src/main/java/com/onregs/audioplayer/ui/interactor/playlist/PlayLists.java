package com.onregs.audioplayer.ui.interactor.playlist;

import android.database.Cursor;

/**
 * Created by vadim on 17.12.2014.
 */
public interface PlayLists
{
    Cursor getPlayLists();
}
