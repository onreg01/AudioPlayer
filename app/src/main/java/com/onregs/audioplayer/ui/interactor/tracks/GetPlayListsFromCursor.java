package com.onregs.audioplayer.ui.interactor.tracks;

import com.onregs.audioplayer.data.PlayList;

import java.util.ArrayList;

/**
 * Created by vadim on 20.12.2014.
 */
public interface GetPlayListsFromCursor
{
    ArrayList<PlayList> getPlayLists();
}
