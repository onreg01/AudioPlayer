package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.fragments.PlayListFragment;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

import org.androidannotations.annotations.EBean;

/**
 * Created by vadim on 11.12.2014.
 */
@EBean
public class PlayListFragmentPresenterImpl implements PlayListFragmentPresenter
{
    PlayListFragment playListFragment;

    @Override
    public void init(BaseView view)
    {
        this.playListFragment = (PlayListFragment) view;
    }

    @Override
    public void restoreSelectedItemColor(OnResumeAction onResumeAction, PlayList playList)
    {
        onResumeAction.restoreSelectedItemColor(this, playList);
    }

    @Override
    public void restoreSelectedItemColor(int position)
    {
        playListFragment.restoreSelectedItemColor(position);
    }
}
