package com.onregs.audioplayer.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.onregs.audioplayer.data.Album;
import com.onregs.audioplayer.ui.adapters.binder.AlbumItemViewBinder;
import com.onregs.audioplayer.ui.adapters.binder.AlbumItemViewBinder_;

import java.util.ArrayList;

/**
 * Created by vadim on 08.12.2014.
 */
public class AlbumsListAdapter extends ArrayAdapter
{

    public AlbumsListAdapter(Context context, int resource, ArrayList<Album> objects)
    {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = AlbumItemViewBinder_.build(getContext());
        }
        Album album = (Album) getItem(position);
        ((AlbumItemViewBinder) convertView).bind(album, getContext());
        return convertView;
    }
}
