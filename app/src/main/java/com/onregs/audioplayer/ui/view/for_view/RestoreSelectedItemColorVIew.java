package com.onregs.audioplayer.ui.view.for_view;

/**
 * Created by vadim on 11.12.2014.
 */
public interface RestoreSelectedItemColorVIew
{
    void restoreSelectedItemColor(int position);
}
