package com.onregs.audioplayer.ui.interactor.resume_view;

import com.onregs.audioplayer.ui.interactor.broadcast_receiver.ReceiverActionListener;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickActionListener;
import com.onregs.audioplayer.ui.presenter.for_presenter.receiver_presenter.RegisterReceiver;

/**
 * Created by vadim on 06.12.2014.
 */
public interface OnResumeActionListener extends ButtonClickActionListener, ReceiverActionListener, RegisterReceiver
{

}
