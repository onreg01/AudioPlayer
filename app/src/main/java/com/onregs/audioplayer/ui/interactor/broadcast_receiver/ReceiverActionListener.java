package com.onregs.audioplayer.ui.interactor.broadcast_receiver;

/**
 * Created by vadim on 05.12.2014.
 */
public interface ReceiverActionListener
{
    void prepareSong(long max, String trackName, String trackAuthor);

    void updateProgress(long milliseconds);
}
