package com.onregs.audioplayer.ui.presenter;

import android.content.ContentResolver;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.widget.ListView;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.Track;
import com.onregs.audioplayer.ui.activity.MusicToPlayList;
import com.onregs.audioplayer.ui.adapters.MusicToPlayListAdapter;
import com.onregs.audioplayer.ui.interactor.tracks.GetAllTracksImpl;
import com.onregs.audioplayer.ui.presenter.for_presenter.BasePresenter;
import com.onregs.audioplayer.ui.view.for_view.BaseView;
import com.onregs.audioplayer.utils.PlayListUtils;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

/**
 * Created by vadim on 23.07.2015.
 */
@EBean
public class MusicToPlayListPresenterImpl implements BasePresenter, MusicToPlayListPresenter
{

    MusicToPlayList musicToPlayList;

    @Override
    public void init(BaseView view)
    {
        musicToPlayList = (MusicToPlayList)view;
    }

    @Override
    public void showMusicList(Context context)
    {
        ArrayList<Track> tracks = new GetAllTracksImpl().getAllTracks(context, null);
        if(!tracks.isEmpty())
        {
            MusicToPlayListAdapter adapter = new MusicToPlayListAdapter(context, R.layout.all_music_item, tracks);
            musicToPlayList.initListView(adapter);
        }
    }

    @Override
    public void addToPlayList(ListView listView, ContentResolver contentResolver,String playListName)
    {
        ArrayList<Track> tracks =  getCheckedTracks(listView);
        Long playListId = PlayListUtils.getPlaylist(contentResolver,playListName);
        PlayListUtils.addToPlaylist(contentResolver,playListId,tracks);

    }

    private  ArrayList<Track> getCheckedTracks(ListView listView)
    {
        ArrayList<Track> tracks = new ArrayList<Track>();
        SparseBooleanArray sbArray = listView.getCheckedItemPositions();
        for (int i = 0; i < sbArray.size(); i++) {
            int key = sbArray.keyAt(i);
            if (sbArray.get(key))
            {
                Track track = (Track) listView.getItemAtPosition(key);
                tracks.add(track);
            }
        }
        return tracks;
    }

}
