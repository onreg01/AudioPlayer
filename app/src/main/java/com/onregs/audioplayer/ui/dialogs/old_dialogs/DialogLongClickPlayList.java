package com.onregs.audioplayer.ui.dialogs.old_dialogs;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.ui.dialogs.BaseDialogInterface;
import com.onregs.audioplayer.utils.PlayListUtils;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

/**
 * Created by vadim on 22.12.2014.
 */
@EFragment(R.layout.dialog_longclick_playlist)
public class DialogLongClickPlayList extends DialogFragment
{
//    public static final String TAG = DialogLongClickPlayList.class.getSimpleName();
//
//    public static DialogLongClickPlayList build(long id, String name)
//    {
//        return DialogLongClickPlayList_.builder().playListId(id).playListName(name).build();
//    }

    @FragmentArg
    long playListId;

    @FragmentArg
    String playListName;

    DialogLongClickPlayListInterface dialogLongClickPlayListInterface;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        dialogLongClickPlayListInterface = (DialogLongClickPlayListInterface) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        getDialog().getWindow().setTitle(playListName);
        getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_color_dialogtitle));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Click
    public void layoutDelete()
    {
        PlayListUtils.deletePlaylist(dialogLongClickPlayListInterface.getResolver(), playListId);
        dialogLongClickPlayListInterface.showMessage(playListName + " " + getResources().getString(R.string.playlist_deleted));
        closeDialogAndUpdateList();
    }

//    @Click
//    public void layoutRename()
//    {
//        DialogFragment dialog = DialogNewPlayList.build(true, playListId, playListName);
//        dialog.show(getFragmentManager(), DialogNewPlayList.TAG);
//        dismiss();
//    }

    private void closeDialogAndUpdateList()
    {
        dialogLongClickPlayListInterface.updateList();
        dismiss();
    }

    public interface DialogLongClickPlayListInterface extends BaseDialogInterface
    {

    }
}
