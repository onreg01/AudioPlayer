package com.onregs.audioplayer.ui.view;

import com.onregs.audioplayer.data.Album;
import com.onregs.audioplayer.ui.view.for_view.ActionView;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

import java.util.ArrayList;

/**
 * Created by vadim on 09.12.2014.
 */
public interface AlbumsListActivityView extends BaseView, ActionView
{
    void initListView(ArrayList<Album> albums);
}
