package com.onregs.audioplayer.ui.activity;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.onregs.audioplayer.Player.Player;
import com.onregs.audioplayer.Player.player_model.PlayerActions;
import com.onregs.audioplayer.Player.player_model.PlayerUtils;
import com.onregs.audioplayer.R;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickAction;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickActionImpl;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeActionIml;
import com.onregs.audioplayer.ui.presenter.FaceTrackActivityPresenter;
import com.onregs.audioplayer.ui.presenter.FaceTrackActivityPresenterImpl;
import com.onregs.audioplayer.ui.view.FaceTrackActivityView;
import com.onregs.audioplayer.ui.view.seek_bar.CustomSeekBar;
import com.onregs.audioplayer.utils.DateUtils;
import com.onregs.audioplayer.utils.ImageUtils;
import com.onregs.audioplayer.utils.StatePlayer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_face_track)
public class FaceTrackActivity extends Activity implements FaceTrackActivityView
{

    @Bean(FaceTrackActivityPresenterImpl.class)
    FaceTrackActivityPresenter presenter;

    @ViewById
    SeekBar seekBar;

    @ViewById(R.id.play)
    ImageView playImage;

    @ViewById
    TextView tvTrackName;

    @ViewById
    TextView tvTrackAuthor;

    @ViewById
    TextView tvCurrentTime;

    @ViewById
    TextView tvTrackDuration;

    @ViewById
    ImageView tvImageTrack;

    @ViewById
    ImageView btnShuffle;

    @ViewById
    ImageView btnRepeat;

    private PlayerActions playerActions;
    private PlayerUtils playerUtils;

    private ButtonClickAction buttonClickAction = new ButtonClickActionImpl(this);
    private OnResumeAction onResumeAction = new OnResumeActionIml(this);

    @AfterViews
    public void init()
    {
        presenter.init(this);
        playerActions = Player.getInstance(this);
        playerUtils = Player.getInstance(this);
        initSeekBar();
    }

    private void initSeekBar()
    {
        seekBar.setOnSeekBarChangeListener(new CustomSeekBar()
        {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
                playerActions.seekTo(seekBar.getProgress());
            }
        });
    }

    @Override
    public void setProgressBar(long max)
    {
        seekBar.setMax((int) max);
        tvTrackDuration.setText(DateUtils.getStringDuration(max));
    }

    @Override
    public void updateProgress(long milliseconds)
    {
        seekBar.setProgress((int) milliseconds);
        tvCurrentTime.setText(DateUtils.getStringDuration(milliseconds));
    }


    @Click
    public void btnShuffle()
    {
        presenter.btnShuffle(buttonClickAction);
    }

    @Click
    public void btnRepeat()
    {
        presenter.btnRepeat(buttonClickAction);
    }

    @Click
    public void previousTrack()
    {
        presenter.btnPreviousTrack(buttonClickAction);
    }

    @Click
    public void nextTrack()
    {
        presenter.btnNextTrack(buttonClickAction);
    }

    @Click
    public void play()
    {
        presenter.btnPlay(buttonClickAction);
    }

    @Override
    public void setTrack (String trackName, String trackAuthor)
    {
        tvTrackName.setText(trackName);
        tvTrackAuthor.setText(trackAuthor);
        tvImageTrack.setImageBitmap(ImageUtils.getBitmapImage(playerUtils.getCurrentTrack().getPath(),this, 1));
    }

    @Override
    public void switchImage(Drawable drawable)
    {
        playImage.setImageDrawable(drawable);
    }

    @Override
    public void updateBtnShuffle(Drawable drawable)
    {
        btnShuffle.setImageDrawable(drawable);
    }

    @Override
    public void updateBtnRepeat(Drawable drawable)
    {
        btnRepeat.setImageDrawable(drawable);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.onResume(onResumeAction);
        presenter.restoreOptionButton(onResumeAction);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        presenter.unregisterReceiver(this);
        StatePlayer.saveOptionButtons(this,playerUtils);
    }

    @Override
    public Activity getContainer()
    {
        return this;
    }

}
