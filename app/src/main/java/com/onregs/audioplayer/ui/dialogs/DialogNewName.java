package com.onregs.audioplayer.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.utils.PlayListUtils;
import com.onregs.audioplayer.utils.edit_text.CustomTextWatcher;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.InstanceState;

/**
 * Created by vadim on 23.12.2014.
 */
@EFragment
public class DialogNewName extends DialogFragment
{
    public static final String TAG = DialogNewName.class.getSimpleName();

    public static DialogNewName build()
    {
        return DialogNewName_.builder().build();
    }

    public static DialogNewName build(boolean isRename, long id, String oldName)
    {
        return DialogNewName_.builder().isRename(isRename).playListId(id).playListName(oldName).build();
    }

    @FragmentArg
    boolean isRename;

    @FragmentArg
    long playListId;

    @FragmentArg
    String playListName;

    EditText editText;

    Button buttonSave;

    @InstanceState
    boolean enabledButton;

    DialogNewNameInterface dialogNewNameInterface;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        dialogNewNameInterface = (DialogNewNameInterface) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_new_name, null);
        editText = (EditText) view.findViewById(R.id.tvEditText);
        editText.addTextChangedListener(textWatcher);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_DARK);
        builder.setTitle("Playlist Name")
                .setView(view)
                .setPositiveButton(getResources().getString(R.string.save), positiveButton)
                .setNegativeButton(getResources().getString(R.string.cancel), null);

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(showListener);
        return dialog;
    }


    CustomTextWatcher textWatcher = new CustomTextWatcher()
    {
        @Override
        public void afterTextChanged(Editable editable)
        {
            if (TextUtils.isEmpty(editable))
            {
                enabledButton = false;
                switchButtonState(false, getResources().getColor(R.color.back));
            }
            else
            {
                enabledButton = true;
                switchButtonState(true, getResources().getColor(R.color.white));
            }
        }
    };

    DialogInterface.OnShowListener showListener = new DialogInterface.OnShowListener()
    {
        @Override
        public void onShow(DialogInterface dialog)
        {
            buttonSave = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
            if (enabledButton)
            {
                switchButtonState(true, getResources().getColor(R.color.white));
            }
            else
            {
                switchButtonState(false, getResources().getColor(R.color.back));
            }
        }
    };


    DialogInterface.OnClickListener positiveButton = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
            String text = editText.getText().toString();
            if (isRename)
            {
                renamePlayList(text);
            }

            else

            {
                createNewPlayList(text);
            }
        }
    };

    private void renamePlayList(String text)
    {
        long state = PlayListUtils.renamePlaylist(dialogNewNameInterface.getResolver(), playListId, text);
        if (state != 0)
        {
            sendMessage(text, getResources().getString(R.string.playlist_renamed));
            closeDialogAndUpdateList();
        }
        else
        {
            sendMessage(text, getResources().getString(R.string.playlist_name_already));
        }
    }

    private void createNewPlayList(String text)
    {
        long state = PlayListUtils.createPlaylist(dialogNewNameInterface.getResolver(), text);

        if (state != 0)
        {
            sendMessage(text, getResources().getString(R.string.playlist_added));
            showAddSongDialog(text);
            closeDialogAndUpdateList();
        }
        else
        {
            sendMessage(text, getResources().getString(R.string.playlist_name_already));
        }
    }

    public void sendMessage(String text, String message)
    {
        dialogNewNameInterface.showMessage(text + " " + message);
    }

    private void closeDialogAndUpdateList()
    {
        dialogNewNameInterface.updateList();
    }


    private void showAddSongDialog(String text)
    {
        Bundle bundle = new Bundle();
        bundle.putString("playListName",text);

        DialogFragment dialog = DialogAddSong.build();
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), DialogAddSong.TAG);
    }

    private void switchButtonState(Boolean stateClickable, int color)
    {
        if (buttonSave != null)
        {
            buttonSave.setClickable(stateClickable);
            buttonSave.setTextColor(color);
        }
    }

    public interface DialogNewNameInterface extends BaseDialogInterface
    {

    }
}
