package com.onregs.audioplayer.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.Track;
import com.onregs.audioplayer.ui.adapters.binder.TrackItemViewBinder;
import com.onregs.audioplayer.ui.adapters.binder.TrackItemViewBinder_;
import com.onregs.audioplayer.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vadim on 30.11.2014.
 */
public class AllMusicListAdapter extends ArrayAdapter
{
    public AllMusicListAdapter(Context context, int resource, ArrayList<Track> tracks)
    {
        super(context, resource, tracks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = TrackItemViewBinder_.build(getContext());
        }
        Track track = (Track) getItem(position);
        ((TrackItemViewBinder) convertView).bind(track, getContext());
        return convertView;
    }
}
