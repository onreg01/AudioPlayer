package com.onregs.audioplayer.ui.interactor.tracks;

import android.content.Context;

import com.onregs.audioplayer.data.Album;
import com.onregs.audioplayer.data.Track;
import com.onregs.audioplayer.data.track_info.AlbumInfo;

import java.util.ArrayList;

/**
 * Created by vadim on 10.12.2014.
 */
public class GetAlbumsWithTracksImpl implements GetAlbumsWithTracks
{
    private Context context;
    private ArrayList<Album> albums;
    private GetAllTracks getAllTracks;

    public GetAlbumsWithTracksImpl(Context context)
    {
        this.context = context;
        albums = new ArrayList<Album>();
        getAllTracks = new GetAllTracksImpl();
    }

    @Override
    public ArrayList<Album> getAlbums()
    {

        ArrayList<Track> tracks = getAllTracks.getAllTracks(context, null);

        for (Track track : tracks)
        {
            AlbumInfo albumInfo = track.getAlbumInfo();
            Album newAlbum = new Album(albumInfo.getAlbumName(), albumInfo.getAlbumId());
            if (!albums.contains(newAlbum))
            {
                newAlbum.setArtistInfo(track.getArtistInfo());
                newAlbum.getPlayList().addTrack(track);
                albums.add(newAlbum);
            }

            else
            {
                for (Album album : albums)
                {
                    if (album.equals(newAlbum))
                    {
                        album.getPlayList().addTrack(track);
                    }
                }
            }
        }
        return albums;
    }
}
