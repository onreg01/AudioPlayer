package com.onregs.audioplayer.ui.view.for_view;

import android.graphics.drawable.Drawable;

/**
 * Created by vadim on 09.12.2014.
 */
public interface ChangeImage
{
    void switchImage(Drawable drawable);
}
