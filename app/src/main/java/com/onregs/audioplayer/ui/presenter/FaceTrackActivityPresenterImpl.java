package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.activity.FaceTrackActivity;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicActionImpl;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

import org.androidannotations.annotations.EBean;

/**
 * Created by vadim on 29.07.2015.
 */
@EBean
public class FaceTrackActivityPresenterImpl extends MusicActionImpl implements FaceTrackActivityPresenter
{

    FaceTrackActivity faceTrackActivity;

    @Override
    public void init(BaseView view)
    {
        super.init(view);
        this.faceTrackActivity  = (FaceTrackActivity) view;
    }


    @Override
    public void restoreOptionButton(OnResumeAction onResumeAction)
    {
        onResumeAction.restoreBtnShuffle(this);
        onResumeAction.restoreBtnRepeat(this);
    }
}
