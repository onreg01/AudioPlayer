package com.onregs.audioplayer.ui.adapters.binder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.onregs.audioplayer.BitmapWorkerTask;
import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.Album;

import org.androidannotations.annotations.EViewGroup;

/**
 * Created by vadim on 08.12.2014.
 */

@EViewGroup(R.layout.all_music_item)
public class AlbumItemViewBinder extends TrackItemViewBinder
{
    public AlbumItemViewBinder(Context context)
    {
        super(context);
    }

    public AlbumItemViewBinder(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public AlbumItemViewBinder(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }


    public View bind(Album album, Context context)
    {
        BitmapWorkerTask task = new BitmapWorkerTask(trackImage, context);
        task.execute(album.getPlayList().getTrack(0).getPath());

        trackAuthor.setText(album.getArtistInfo().getArtistName()+" "+album.getPlayList().getSize()+" songs");
        trackName.setText(album.getAlbumName());
        trackDuration.setVisibility(View.GONE);
        return this;
    }
}
