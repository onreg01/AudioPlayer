package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.presenter.for_presenter.BasePresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.RestoreSelectedItemColorPresenter;

/**
 * Created by vadim on 11.12.2014.
 */
public interface PlayListFragmentPresenter extends BasePresenter, RestoreSelectedItemColorPresenter
{

}
