package com.onregs.audioplayer.ui.interactor.broadcast_receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


/**
 * Created by vadim on 05.12.2014.
 */
public class ProgressReceiverIml extends BroadcastReceiver implements ProgressReceiver
{

    public final static int PREPARE_SONG = 100;
    public final static int UPDATE_PROGRESS = 200;

    public final static String STATUS = "status";
    public final static String DATA = "data";
    public final static String TRACK_NAME = "track_name";
    public final static String TRACK_AUTHOR = "track_author";

    public final static String BROADCAST_ACTION = "com.onregs.audioplayer.progress";


    private ReceiverActionListener actionListener;

    public ProgressReceiverIml(ReceiverActionListener actionListener)
    {
        this.actionListener = actionListener;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        int status = intent.getIntExtra(STATUS, 0);

        if (status == UPDATE_PROGRESS)
        {
            long data = intent.getLongExtra(DATA, 0);
            actionListener.updateProgress(data);
        }

        else if (status == PREPARE_SONG)
        {
            long data = intent.getLongExtra(DATA, 0);
            String trackName = intent.getStringExtra(TRACK_NAME);
            String trackAuthor = intent.getStringExtra(TRACK_AUTHOR);
            actionListener.prepareSong(data, trackName,trackAuthor);
        }
    }

    @Override
    public void initReceiver(Context context)
    {
        IntentFilter filter = new IntentFilter(BROADCAST_ACTION);
        context.registerReceiver(this, filter);
    }

    @Override
    public void removeReceiver(Context context)
    {
        context.unregisterReceiver(this);
    }
}
