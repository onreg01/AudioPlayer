package com.onregs.audioplayer.ui.adapters.binder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.Artist;

import org.androidannotations.annotations.EViewGroup;

/**
 * Created by vadim on 14.12.2014.
 */
@EViewGroup(R.layout.all_music_item)
public class ArtistItemViewBinder extends TrackItemViewBinder
{
    public ArtistItemViewBinder(Context context)
    {
        super(context);
    }

    public ArtistItemViewBinder(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public ArtistItemViewBinder(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public View bind(Artist artist, Context context)
    {
        trackImage.setVisibility(View.GONE);
        trackDuration.setVisibility(View.GONE);

        trackName.setText(artist.getArtistName());

        trackAuthor.setText(getTypeAlbum(artist.getAlbumsCount()));

        return this;
    }

    private String getTypeAlbum(int size)
    {
        if (size == 1)
        {
            return size + " Album";
        }
        return size + " Albums";
    }
}
