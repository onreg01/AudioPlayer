package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.activity.AlbumsListActivity;
import com.onregs.audioplayer.ui.interactor.tracks.GetAlbumsWithTracks;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicActionImpl;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

import org.androidannotations.annotations.EBean;

/**
 * Created by vadim on 09.12.2014.
 */

@EBean
public class AlbumsListActivityPresenterImpl extends MusicActionImpl implements AlbumsListActivityPresenter
{

    AlbumsListActivity view;

    @Override
    public void init(BaseView view)
    {
        super.init(view);
        this.view = (AlbumsListActivity)view;
    }

    @Override
    public void setListViewAlbums(GetAlbumsWithTracks getAlbumsWithTracks)
    {
        view.initListView(getAlbumsWithTracks.getAlbums());
    }
}
