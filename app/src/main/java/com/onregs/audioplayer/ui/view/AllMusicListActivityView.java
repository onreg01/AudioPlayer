package com.onregs.audioplayer.ui.view;

import com.onregs.audioplayer.ui.view.for_view.ActionView;
import com.onregs.audioplayer.ui.view.for_view.BaseView;
import com.onregs.audioplayer.ui.view.for_view.RestoreSelectedItemColorVIew;

/**
 * Created by vadim on 02.12.2014.
 */
public interface AllMusicListActivityView extends BaseView, ActionView, RestoreSelectedItemColorVIew
{

}
