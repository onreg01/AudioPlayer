package com.onregs.audioplayer.ui.presenter.for_presenter;

import com.onregs.audioplayer.ui.view.for_view.BaseView;

/**
 * Created by vadim on 20.11.2014.
 */
public interface BasePresenter <T extends BaseView>
{
    void init(BaseView view);
}