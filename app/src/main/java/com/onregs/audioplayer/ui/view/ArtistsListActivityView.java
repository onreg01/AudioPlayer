package com.onregs.audioplayer.ui.view;

import com.onregs.audioplayer.data.Artist;
import com.onregs.audioplayer.ui.view.for_view.ActionView;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

import java.util.ArrayList;

/**
 * Created by vadim on 14.12.2014.
 */
public interface ArtistsListActivityView extends BaseView, ActionView
{
    void initListView(ArrayList<Artist> artists);
}
