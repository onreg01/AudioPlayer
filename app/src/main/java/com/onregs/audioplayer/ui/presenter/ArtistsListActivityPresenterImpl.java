package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.activity.ArtistsListActivity;
import com.onregs.audioplayer.ui.interactor.tracks.GetArtistsList;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicActionImpl;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

import org.androidannotations.annotations.EBean;

/**
 * Created by vadim on 14.12.2014.
 */

@EBean
public class ArtistsListActivityPresenterImpl extends MusicActionImpl implements ArtistsListActivityPresenter
{
    ArtistsListActivity ArtistsListActivity;

    @Override
    public void init(BaseView view)
    {
        super.init(view);
        ArtistsListActivity = (ArtistsListActivity) view;
    }

    @Override
    public void setListViewArtists(GetArtistsList getArtistsList)
    {
        ArtistsListActivity.initListView(getArtistsList.getArtists());
    }
}
