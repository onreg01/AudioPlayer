package com.onregs.audioplayer.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.adapters.binder.PlayListItemViewBinder;
import com.onregs.audioplayer.ui.adapters.binder.PlayListItemViewBinder_;

import java.util.ArrayList;

/**
 * Created by vadim on 20.12.2014.
 */
public class PlayListsAdapter extends ArrayAdapter
{
    public PlayListsAdapter(Context context, ArrayList<PlayList> playLists)
    {
        super(context, 0, playLists);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        if(convertView==null)
        {
            convertView = PlayListItemViewBinder_.build(getContext());
        }

        PlayList playList = (PlayList) getItem(position);
        ((PlayListItemViewBinder)convertView).bind(playList);
        return convertView;
    }
}
