package com.onregs.audioplayer.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.onregs.audioplayer.data.Artist;
import com.onregs.audioplayer.ui.adapters.binder.ArtistItemViewBinder;
import com.onregs.audioplayer.ui.adapters.binder.ArtistItemViewBinder_;

import java.util.ArrayList;

/**
 * Created by vadim on 14.12.2014.
 */
public class ArtistListAdapter extends ArrayAdapter
{
    public ArtistListAdapter(Context context, int resource, ArrayList<Artist> objects)
    {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = ArtistItemViewBinder_.build(getContext());
        }
        Artist artist = (Artist) getItem(position);
        ((ArtistItemViewBinder) convertView).bind(artist, getContext());
        return convertView;
    }
}
