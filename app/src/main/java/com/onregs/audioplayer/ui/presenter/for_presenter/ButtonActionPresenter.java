package com.onregs.audioplayer.ui.presenter.for_presenter;

import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickAction;

/**
 * Created by vadim on 05.12.2014.
 */
public interface ButtonActionPresenter
{

    void btnPreviousTrack(ButtonClickAction buttonClickAction);

    void btnNextTrack(ButtonClickAction buttonClickAction);

    void btnPlay(ButtonClickAction buttonClickAction);

    void btnShuffle(ButtonClickAction buttonClickAction);

    void btnRepeat(ButtonClickAction buttonClickAction);
}
