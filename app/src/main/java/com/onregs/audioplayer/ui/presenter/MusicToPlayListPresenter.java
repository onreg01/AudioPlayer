package com.onregs.audioplayer.ui.presenter;

import android.content.ContentResolver;
import android.content.Context;
import android.widget.ListView;

/**
 * Created by vadim on 23.07.2015.
 */
public interface MusicToPlayListPresenter
{
    public void showMusicList(Context context);
    public void addToPlayList(ListView listView, ContentResolver contentResolver,String playListName);
}
