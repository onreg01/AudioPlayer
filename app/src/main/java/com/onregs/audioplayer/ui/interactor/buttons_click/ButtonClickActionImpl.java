package com.onregs.audioplayer.ui.interactor.buttons_click;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.onregs.audioplayer.Player.Player;
import com.onregs.audioplayer.Player.player_model.PlayerActions;
import com.onregs.audioplayer.Player.player_model.PlayerUtils;
import com.onregs.audioplayer.R;

/**
 * Created by vadim on 05.12.2014.
 */
public class ButtonClickActionImpl implements ButtonClickAction
{
    private Context context;
    private PlayerActions playerActions;
    private PlayerUtils playerUtils;

    public ButtonClickActionImpl(Context context)
    {
        this.context = context;
        playerActions = Player.getInstance(context);
        playerUtils = Player.getInstance(context);
    }

    @Override
    public void previousTrack(ButtonClickActionListener listener)
    {
        if (checkPlayer() || checkStateAfterExit())
        {
            if (playerUtils.getCurrentPlaylist() != null)
            {
                playerActions.previousTrack();
                if (!checkPlaying())
                {
                    listener.switchImagePlay(getPauseImage());
                }
            }
        }
    }

    @Override
    public void nextTrack(ButtonClickActionListener listener)
    {
        if (checkPlayer() || checkStateAfterExit())
        {
            if (playerUtils.getCurrentPlaylist() != null)
            {
                playerActions.nextTrack();
                if (!checkPlaying())
                {
                    listener.switchImagePlay(getPauseImage());
                }
            }
        }
        else if (checkStateAfterExit() && checkCurrentPlayList())
        {
            playerActions.nextTrack();
            if (!checkPlaying())
            {
                listener.switchImagePlay(getPauseImage());
            }
        }
    }

    @Override
    public void play(ButtonClickActionListener listener)
    {
        if (checkPlayer())
        {
            if (checkPlaying())
            {
                playerActions.pause();
                listener.switchImagePlay(context.getResources().getDrawable(R.drawable.ic_action_play));
            }
            else
            {
                playerActions.unPause();
                listener.switchImagePlay(getPauseImage());
            }
        }
        else if (checkStateAfterExit() && checkCurrentPlayList())
        {
            playerActions.start(playerUtils.getPosition());
            listener.switchImagePlay(getPauseImage());
        }
    }

    @Override
    public void shuffle(ButtonClickActionListener listener)
    {

        boolean state = playerUtils.isShuffle();
        if(state == true)
        {
            playerActions.shuffle(false);
            listener.switchImageShuffle(context.getResources().getDrawable(R.drawable.ic_action_do_not_shuffle));
        }
        else
        {
            playerActions.shuffle(true);
            listener.switchImageShuffle(context.getResources().getDrawable(R.drawable.ic_action_shuffle));
        }


    }

    @Override
    public void repeat(ButtonClickActionListener listener)
    {
        boolean state = playerUtils.isRepeat();
        if(state == true)
        {
            playerActions.repeat(false);
            listener.switchImageRepeat(context.getResources().getDrawable(R.drawable.ic_action_repeat_nothing));
        }
        else
        {
            playerActions.repeat(true);
            listener.switchImageRepeat(context.getResources().getDrawable(R.drawable.ic_action_repeat));
        }
    }

    private boolean checkPlayer()
    {
        return playerUtils.getPlayer() != null;
    }

    private boolean checkPlaying()
    {
        return playerUtils.getPlayer().isPlaying();
    }

    private Drawable getPauseImage()
    {
        return context.getResources().getDrawable(R.drawable.ic_action_pause);
    }

    private boolean checkStateAfterExit()
    {
        return playerUtils.getState();
    }

    private boolean checkCurrentPlayList()
    {
        return playerUtils.getCurrentPlaylist() != null;
    }

}
