package com.onregs.audioplayer.ui.adapters.binder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.PlayList;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vadim on 19.12.2014.
 */
@EViewGroup(R.layout.playlists_item)
public class PlayListItemViewBinder extends LinearLayout
{
    @ViewById(R.id.tvPlayListName)
    TextView playListName;

    private long playListId;

    private String playListNameField;

    public PlayListItemViewBinder(Context context)
    {
        super(context);
    }

    public PlayListItemViewBinder(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public PlayListItemViewBinder(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public View bind(PlayList playList)
    {
        playListId = playList.getPlayListId();
        playListNameField = playList.getPlaylistName();

        playListName.setText(playListNameField);

        return this;
    }

    public long getPlayListId()
    {
        return playListId;
    }

    public String getPlayListName()
    {
        return playListNameField;
    }
}
