package com.onregs.audioplayer.ui.view.for_view;

import android.graphics.drawable.Drawable;

/**
 * Created by vadim on 30.07.2015.
 */
public interface FaceTrackActivityActionView extends ActionView
{
    void updateBtnShuffle(Drawable drawable);

    void updateBtnRepeat(Drawable drawable);
}
