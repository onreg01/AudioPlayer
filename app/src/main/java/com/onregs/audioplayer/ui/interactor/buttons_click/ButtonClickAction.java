package com.onregs.audioplayer.ui.interactor.buttons_click;

/**
 * Created by vadim on 05.12.2014.
 */
public interface ButtonClickAction
{
    void previousTrack(ButtonClickActionListener listener);

    void nextTrack(ButtonClickActionListener listener);

    void play(ButtonClickActionListener listener);

    void shuffle(ButtonClickActionListener listener);

    void repeat(ButtonClickActionListener listener);
}
