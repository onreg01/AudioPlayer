package com.onregs.audioplayer.ui.activity;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.Album;
import com.onregs.audioplayer.data.Artist;
import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.adapters.ArtistListAdapter;
import com.onregs.audioplayer.ui.fragments.ArtistAlbumsListFragment;
import com.onregs.audioplayer.ui.fragments.ArtistAlbumsListFragment_;
import com.onregs.audioplayer.ui.fragments.PlayListFragment;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickAction;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickActionImpl;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeActionIml;
import com.onregs.audioplayer.ui.interactor.tracks.GetArtistsListImpl;
import com.onregs.audioplayer.ui.presenter.ArtistsListActivityPresenter;
import com.onregs.audioplayer.ui.presenter.ArtistsListActivityPresenterImpl;
import com.onregs.audioplayer.ui.view.ArtistsListActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_artists_list)
public class ArtistsListActivity extends Activity implements
        ArtistsListActivityView, ArtistAlbumsListFragment.ArtistAlbumsListFragmentInterface, PlayListFragment.PlayListFragmentInterface
{


    @Bean(ArtistsListActivityPresenterImpl.class)
    ArtistsListActivityPresenter presenter;

    @ViewById(R.id.lvAlbums)
    ListView listView;

    @ViewById(R.id.progressBar)
    ProgressBar progressBar;

    @ViewById(R.id.play)
    ImageView playImage;

    @ViewById(R.id.tvTrackName)
    TextView tvTrackName;

    @InstanceState
    int position;

    @InstanceState
    int albumPosition;

    private ArrayList<Artist> artists;

    private ButtonClickAction buttonClickAction = new ButtonClickActionImpl(this);
    private OnResumeAction onResumeAction = new OnResumeActionIml(this);

    @AfterViews
    public void init()
    {
        presenter.init(this);
        presenter.setListViewArtists(new GetArtistsListImpl(this));
    }

    @Override
    public void initListView(ArrayList<Artist> artists)
    {
        this.artists = artists;
        ArtistListAdapter artistListAdapter = new ArtistListAdapter(this, R.layout.all_music_item, artists);
        listView.setAdapter(artistListAdapter);
        setListener();
    }

    private void setListener()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id)
            {
                position = pos;
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.fragmentContainer, ArtistAlbumsListFragment_.build(), ArtistAlbumsListFragment.TAG);
                ft.addToBackStack(ArtistAlbumsListFragment.TAG);
                ft.commit();
            }
        });
    }


    @Override
    public void setProgressBar(long max)
    {
        progressBar.setMax((int) max);
    }

    @Override
    public void updateProgress(long milliseconds)
    {
        progressBar.setProgress((int) milliseconds);
    }

    @Override
    public Activity getContainer()
    {
        return this;
    }

    @Click
    public void previousTrack()
    {
        presenter.btnPreviousTrack(buttonClickAction);
    }

    @Click
    public void nextTrack()
    {
        presenter.btnNextTrack(buttonClickAction);
    }

    @Override
    public void switchImage(Drawable drawable)
    {
        playImage.setImageDrawable(drawable);
    }

    @Override
    public void setTrack (String trackName, String trackAuthor)
    {
        tvTrackName.setText(trackName);
    }

    @Click
    public void play()
    {
        presenter.btnPlay(buttonClickAction);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.onResume(onResumeAction);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        presenter.unregisterReceiver(this);
    }

    @Override
    public ArrayList<Album> getAlbumsList()
    {
        return artists.get(position).getAlbums();
    }

    @Override
    public void setAlbumPosition(int albumPosition)
    {
        this.albumPosition = albumPosition;
    }

    @Override
    public PlayList getPlayList()
    {
        return artists.get(position).getAlbums().get(albumPosition).getPlayList();
    }

    @Override
    public void changeImage(Drawable drawable)
    {
        switchImage(drawable);
    }
}
