package com.onregs.audioplayer.ui.interactor.buttons_click;

import android.graphics.drawable.Drawable;

/**
 * Created by vadim on 05.12.2014.
 */
public interface ButtonClickActionListener
{
    void switchImagePlay(Drawable drawable);

    void switchImageShuffle(Drawable drawable);

    void switchImageRepeat(Drawable drawable);
}
