package com.onregs.audioplayer.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.utils.Luncher;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vadim on 23.12.2014.
 */

@EFragment
public class DialogAddSong extends DialogFragment
{
    public static final String TAG = DialogAddSong.class.getSimpleName();

    public static DialogAddSong build()
    {
        return DialogAddSong_.builder().build();
    }

    private String playListName;

    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        playListName = getArguments().getString("playListName");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_DARK);
        builder.setTitle("Add songs")
                .setMessage("Add songs to playlist")
                .setPositiveButton(getResources().getString(R.string.ok), positiveButton)
                .setNegativeButton(getResources().getString(R.string.cancel), null);

        AlertDialog dialog = builder.create();
        return dialog;
    }

    DialogInterface.OnClickListener positiveButton = new DialogInterface.OnClickListener()
    {
        @Override
        public void onClick(DialogInterface dialogInterface, int i)
        {
            Luncher.startMusicToPlayListActivity(getActivity(),playListName);
        }
    };
}
