package com.onregs.audioplayer.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.onregs.audioplayer.data.Track;
import com.onregs.audioplayer.ui.adapters.binder.MusicToPlayListItemBinder_;
import com.onregs.audioplayer.ui.adapters.binder.TrackItemViewBinder;

import java.util.ArrayList;

/**
 * Created by vadim on 23.07.2015.
 */
public class MusicToPlayListAdapter extends ArrayAdapter
{
    public MusicToPlayListAdapter(Context context, int resource, ArrayList<Track> tracks)
    {
        super(context, resource, tracks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = MusicToPlayListItemBinder_.build(getContext());
        }
        Track track = (Track) getItem(position);
        ((TrackItemViewBinder) convertView).bind(track, getContext());
        return convertView;
    }
}