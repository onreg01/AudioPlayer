package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.activity.PlayListsActivity;
import com.onregs.audioplayer.ui.interactor.tracks.GetPlayListsFromCursor;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicActionImpl;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

import org.androidannotations.annotations.EBean;

/**
 * Created by vadim on 17.12.2014.
 */

@EBean
public class PlayListsActivityPresenterImpl extends MusicActionImpl implements PlayListsActivityPresenter
{
    PlayListsActivity playListsActivity;

    @Override
    public void init(BaseView view)
    {
        super.init(view);
        playListsActivity = (PlayListsActivity) view;
    }

    @Override
    public void showAllPlayLists(GetPlayListsFromCursor getPlayListsFromCursor)
    {
        playListsActivity.showPlayLists(getPlayListsFromCursor.getPlayLists());
    }
}
