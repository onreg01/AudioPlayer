package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.activity.AllMusicListActivity;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicActionImpl;
import com.onregs.audioplayer.ui.view.for_view.BaseView;

import org.androidannotations.annotations.EBean;

/**
 * Created by vadim on 02.12.2014.
 */
@EBean
public class AllMusicListActivityPresenterImpl extends MusicActionImpl implements AllMusicListActivityPresenter
{
    AllMusicListActivity allMusicListActivity;

    @Override
    public void init(BaseView view)
    {
        super.init(view);
        allMusicListActivity = (AllMusicListActivity) view;
    }

    @Override
    public void restoreSelectedItemColor(OnResumeAction onResumeAction, PlayList playList)
    {
        onResumeAction.restoreSelectedItemColor(this, playList);
    }

    @Override
    public void restoreSelectedItemColor(int position)
    {
        allMusicListActivity.restoreSelectedItemColor(position);
    }
}
