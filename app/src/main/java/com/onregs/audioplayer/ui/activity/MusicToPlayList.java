package com.onregs.audioplayer.ui.activity;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.ui.presenter.MusicToPlayListPresenterImpl;
import com.onregs.audioplayer.ui.view.MusicToPlayListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_music_to_play_list)
public class MusicToPlayList extends Activity implements MusicToPlayListView
{
    @Bean(MusicToPlayListPresenterImpl.class)
    MusicToPlayListPresenterImpl presenter;

    @ViewById(R.id.lvAllMusic)
    ListView lvAllMusic;

    @Extra
    String playListName;

    @AfterViews
    public void init()
    {
        presenter.init(this);
        presenter.showMusicList(this);
    }

    @Override
    public void initListView(ArrayAdapter adapter)
    {
        lvAllMusic.setAdapter(adapter);
    }

    @Override
    public Activity getContainer()
    {
        return this;
    }

    @Click(R.id.tvAdd)
    public void addButton()
    {
        presenter.addToPlayList(lvAllMusic, getContentResolver(),playListName);
        setResult(1);
        finish();
    }

    @Click(R.id.tvCancel)
    public void cancelButton()
    {
        setResult(0);
        finish();
    }
}
