package com.onregs.audioplayer.ui.interactor.resume_view;

import com.onregs.audioplayer.data.PlayList;

/**
 * Created by vadim on 06.12.2014.
 */
public interface OnResumeAction
{
    void onResume(OnResumeActionListener onResumeActionListener);

    void restoreSelectedItemColor(RestoreSelectedItemColorListener restoreSelectedItemColor,  PlayList playList);

    void restoreBtnShuffle(RestoreOptionButtonsState restoreOptionButtonsState);

    void restoreBtnRepeat(RestoreOptionButtonsState restoreOptionButtonsState);
}
