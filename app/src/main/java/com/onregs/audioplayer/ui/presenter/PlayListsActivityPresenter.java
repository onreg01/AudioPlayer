package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.interactor.tracks.GetPlayListsFromCursor;
import com.onregs.audioplayer.ui.presenter.for_presenter.BasePresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicAction;

/**
 * Created by vadim on 17.12.2014.
 */
public interface PlayListsActivityPresenter extends BasePresenter, MusicAction
{
    void showAllPlayLists(GetPlayListsFromCursor getPlayListsFromCursor);
}
