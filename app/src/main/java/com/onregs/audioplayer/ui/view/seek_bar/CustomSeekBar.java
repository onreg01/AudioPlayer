package com.onregs.audioplayer.ui.view.seek_bar;

import android.widget.SeekBar;

/**
 * Created by vadim on 03.08.2015.
 */
public abstract class CustomSeekBar implements SeekBar.OnSeekBarChangeListener
{
    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b)
    {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar)
    {

    }
}
