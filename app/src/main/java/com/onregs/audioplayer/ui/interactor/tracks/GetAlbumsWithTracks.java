package com.onregs.audioplayer.ui.interactor.tracks;

import com.onregs.audioplayer.data.Album;

import java.util.ArrayList;

/**
 * Created by vadim on 08.12.2014.
 */
public interface GetAlbumsWithTracks
{
    ArrayList<Album> getAlbums ();
}
