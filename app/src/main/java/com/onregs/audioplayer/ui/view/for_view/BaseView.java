package com.onregs.audioplayer.ui.view.for_view;

import android.app.Activity;

/**
 * Created by vadim on 20.11.2014.
 */
public interface BaseView
{
    Activity getContainer();
}
