package com.onregs.audioplayer.ui.adapters.binder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onregs.audioplayer.BitmapWorkerTask;
import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.Track;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vadim on 02.12.2014.
 */

@EViewGroup(R.layout.all_music_item)
public class TrackItemViewBinder extends LinearLayout
{
    @ViewById(R.id.trackAuthor)
    TextView trackAuthor;

    @ViewById(R.id.trackName)
    TextView trackName;

    @ViewById(R.id.trackDuration)
    TextView trackDuration;

    @ViewById(R.id.trackImage)
    ImageView trackImage;

    int i = 0;

    public TrackItemViewBinder(Context context)
    {
        super(context);
    }

    public TrackItemViewBinder(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public TrackItemViewBinder(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public View bind(Track track, Context context)
    {
        BitmapWorkerTask task = new BitmapWorkerTask(trackImage, context);
        task.execute(track.getPath());

        trackAuthor.setText(track.getArtistInfo().getArtistName());
        trackName.setText(track.getTrackName());
        trackDuration.setText(track.getDuration().getStringDuration());

        return this;
    }
}
