package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.presenter.for_presenter.BasePresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.RestoreSelectedItemColorPresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicAction;

/**
 * Created by vadim on 02.12.2014.
 */
public interface AllMusicListActivityPresenter extends BasePresenter, MusicAction, RestoreSelectedItemColorPresenter
{

}
