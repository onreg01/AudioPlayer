package com.onregs.audioplayer.ui.activity;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.Album;
import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.adapters.AlbumsListAdapter;
import com.onregs.audioplayer.ui.fragments.PlayListFragment;
import com.onregs.audioplayer.ui.fragments.PlayListFragment_;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickAction;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickActionImpl;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeActionIml;
import com.onregs.audioplayer.ui.interactor.tracks.GetAlbumsWithTracksImpl;
import com.onregs.audioplayer.ui.presenter.AlbumsListActivityPresenter;
import com.onregs.audioplayer.ui.presenter.AlbumsListActivityPresenterImpl;
import com.onregs.audioplayer.ui.view.AlbumsListActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_albums_list)
public class AlbumsListActivity extends Activity implements AlbumsListActivityView, PlayListFragment.PlayListFragmentInterface
{

    @Bean(AlbumsListActivityPresenterImpl.class)
    AlbumsListActivityPresenter presenter;

    @ViewById(R.id.lvAlbums)
    ListView listView;

    @ViewById(R.id.lvAllMusic)
    ListView lvAllMusic;

    @ViewById(R.id.progressBar)
    ProgressBar progressBar;

    @ViewById(R.id.play)
    ImageView playImage;

    @ViewById(R.id.tvTrackName)
    TextView tvTrackName;

    private ButtonClickAction buttonClickAction = new ButtonClickActionImpl(this);
    private OnResumeAction onResumeAction = new OnResumeActionIml(this);

    private ArrayList<Album> albums;

    @InstanceState
    int position;

    @AfterViews
    public void init()
    {
        presenter.init(this);
        presenter.setListViewAlbums(new GetAlbumsWithTracksImpl(this));
    }

    @Override
    public void initListView(ArrayList<Album> albums)
    {
        this.albums = albums;
        ArrayAdapter arrayAdapter = new AlbumsListAdapter(getContainer(), R.layout.all_music_item, albums);
        listView.setAdapter(arrayAdapter);
        setListener();
    }

    private void setListener()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id)
            {
                position = pos;
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.fragmentContainer, PlayListFragment_.build(), PlayListFragment.TAG);
                ft.addToBackStack(PlayListFragment.TAG);
                ft.commit();
            }
        });
    }

    @Override
    public void setProgressBar(long max)
    {
        progressBar.setMax((int) max);
    }

    @Override
    public void updateProgress(long milliseconds)
    {
        progressBar.setProgress((int) milliseconds);
    }

    @Override
    public Activity getContainer()
    {
        return this;
    }

    @Click
    public void previousTrack()
    {
        presenter.btnPreviousTrack(buttonClickAction);
    }

    @Click
    public void nextTrack()
    {
        presenter.btnNextTrack(buttonClickAction);
    }

    @Override
    public void switchImage(Drawable drawable)
    {
        playImage.setImageDrawable(drawable);
    }

    @Override
    public void setTrack (String trackName, String trackAuthor)
    {
        tvTrackName.setText(trackName);
    }

    @Click
    public void play()
    {
        presenter.btnPlay(buttonClickAction);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.onResume(onResumeAction);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        presenter.unregisterReceiver(this);
    }

    @Override
    public PlayList getPlayList()
    {
        return albums.get(position).getPlayList();
    }

    @Override
    public void changeImage(Drawable drawable)
    {
        switchImage(drawable);
    }
}
