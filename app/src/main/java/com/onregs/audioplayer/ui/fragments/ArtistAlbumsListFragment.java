package com.onregs.audioplayer.ui.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.Album;
import com.onregs.audioplayer.ui.adapters.AlbumsListAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EFragment(R.layout.fragment_artist_albums_list)
public class ArtistAlbumsListFragment extends Fragment
{
    public static final String TAG = ArtistAlbumsListFragment.class.getSimpleName();

    public static ArtistAlbumsListFragment build()
    {
        return ArtistAlbumsListFragment_.builder().build();
    }

    @ViewById(R.id.lvAlbums)
    ListView listView;

    private ArrayList<Album> albums;

    @AfterViews
    public void initListView()
    {
        albums = artistAlbumsListFragmentInterface.getAlbumsList();
        AlbumsListAdapter albumsListAdapter = new AlbumsListAdapter(getActivity(), R.layout.all_music_item, albums);
        listView.setAdapter(albumsListAdapter);
        setListener();
    }

    private void setListener()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id)
            {

                artistAlbumsListFragmentInterface.setAlbumPosition(pos);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.fragmentContainer, PlayListFragment_.build(), PlayListFragment.TAG);
                ft.addToBackStack(PlayListFragment.TAG);
                ft.commit();
            }
        });
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        artistAlbumsListFragmentInterface = (ArtistAlbumsListFragmentInterface) activity;
    }

    ArtistAlbumsListFragmentInterface artistAlbumsListFragmentInterface;

    public interface ArtistAlbumsListFragmentInterface
    {
        ArrayList<Album> getAlbumsList();
        void setAlbumPosition(int albumPosition);
    }

    private ArtistAlbumsListFragment getCurrentFragment()
    {
        return this;
    }

}
