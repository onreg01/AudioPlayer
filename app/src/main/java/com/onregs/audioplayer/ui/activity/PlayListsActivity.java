package com.onregs.audioplayer.ui.activity;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.ui.adapters.PlayListsAdapter;
import com.onregs.audioplayer.ui.adapters.binder.PlayListItemViewBinder;
import com.onregs.audioplayer.ui.dialogs.DialogLongClickPlayLists;
import com.onregs.audioplayer.ui.dialogs.DialogNewName;
import com.onregs.audioplayer.ui.fragments.PlayListFragment;
import com.onregs.audioplayer.ui.fragments.PlayListFragment_;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickAction;
import com.onregs.audioplayer.ui.interactor.buttons_click.ButtonClickActionImpl;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeAction;
import com.onregs.audioplayer.ui.interactor.resume_view.OnResumeActionIml;
import com.onregs.audioplayer.ui.interactor.tracks.GetPlayListsFromCursorImpl;
import com.onregs.audioplayer.ui.presenter.PlayListsActivityPresenter;
import com.onregs.audioplayer.ui.presenter.PlayListsActivityPresenterImpl;
import com.onregs.audioplayer.ui.view.PlayListsActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_play_lists)
public class PlayListsActivity extends Activity implements
        PlayListsActivityView, PlayListFragment.PlayListFragmentInterface,
        DialogNewName.DialogNewNameInterface, DialogLongClickPlayLists.DialogLongClickPlayListsInterface
{

    @Bean(PlayListsActivityPresenterImpl.class)
    PlayListsActivityPresenter presenter;

    @ViewById(R.id.lvPlayLists)
    ListView listView;

    @ViewById(R.id.progressBar)
    ProgressBar progressBar;

    @ViewById(R.id.play)
    ImageView playImage;

    @ViewById(R.id.tvTrackName)
    TextView tvTrackName;

    @InstanceState
    int currentPlayList;

    private ArrayList<PlayList> playLists;

    private ButtonClickAction buttonClickAction = new ButtonClickActionImpl(this);
    private OnResumeAction onResumeAction = new OnResumeActionIml(this);

    @AfterViews
    public void init()
    {
        presenter.init(this);
        initList();
    }


    private void initList()
    {
        presenter.showAllPlayLists(new GetPlayListsFromCursorImpl(this));
    }


    @Override
    public void showPlayLists(ArrayList<PlayList> playLists)
    {
        this.playLists = playLists;
        PlayListsAdapter adapter = new PlayListsAdapter(this, playLists);
        listView.setAdapter(adapter);
        setItemClickListener();
        setItemLongClockListener();
    }

    private void setItemClickListener()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                currentPlayList = position;
                showPlayListFragment();
            }
        });
    }


    private void setItemLongClockListener()
    {
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l)
            {

                String name = ((PlayListItemViewBinder) view).getPlayListName();
                Long id = ((PlayListItemViewBinder) view).getPlayListId();

                DialogFragment dialogFragment = DialogLongClickPlayLists.build(id, name);
                dialogFragment.show(getFragmentManager(), DialogLongClickPlayLists.TAG);
                return true;
            }
        });
    }

    private void showPlayListFragment()
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainer, PlayListFragment_.build(), PlayListFragment.TAG);
        ft.addToBackStack(PlayListFragment.TAG);
        ft.commit();
    }


    @Click
    public void newPlayList()
    {
        DialogFragment dialogFragment = DialogNewName.build();
        dialogFragment.show(getFragmentManager(), DialogNewName.TAG);
    }

    @Override
    public void setProgressBar(long max)
    {
        progressBar.setMax((int) max);
    }

    @Override
    public void updateProgress(long milliseconds)
    {
        progressBar.setProgress((int) milliseconds);
    }

    @Override
    public Activity getContainer()
    {
        return this;
    }

    @Click
    public void previousTrack()
    {
        presenter.btnPreviousTrack(buttonClickAction);
    }

    @Click
    public void nextTrack()
    {
        presenter.btnNextTrack(buttonClickAction);
    }

    @Override
    public void switchImage(Drawable drawable)
    {
        playImage.setImageDrawable(drawable);
    }

    @Override
    public void setTrack (String trackName, String trackAuthor)
    {
        tvTrackName.setText(trackName);
    }

    @Click
    public void play()
    {
        presenter.btnPlay(buttonClickAction);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        initList();
        presenter.onResume(onResumeAction);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        presenter.unregisterReceiver(this);
    }

    @Override
    public PlayList getPlayList()
    {
        return playLists.get(currentPlayList);
    }

    @Override
    public void changeImage(Drawable drawable)
    {
        switchImage(drawable);
    }


    @Override
    public void showMessage(String message)
    {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateList()
    {
        initList();
    }

    @Override
    public ContentResolver getResolver()
    {
        return getContentResolver();
    }

    @OnActivityResult(1)
    void onResult(int resultCode)
    {
        if (resultCode == 1)
        {
            initList();
            currentPlayList = playLists.size()-1;
            showPlayListFragment();
        }
    }

}
