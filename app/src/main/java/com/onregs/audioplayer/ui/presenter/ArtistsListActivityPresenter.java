package com.onregs.audioplayer.ui.presenter;

import com.onregs.audioplayer.ui.interactor.tracks.GetArtistsList;
import com.onregs.audioplayer.ui.presenter.for_presenter.BasePresenter;
import com.onregs.audioplayer.ui.presenter.for_presenter.bot_line_actions.MusicAction;

/**
 * Created by vadim on 14.12.2014.
 */
public interface ArtistsListActivityPresenter extends BasePresenter, MusicAction
{
    public void setListViewArtists(GetArtistsList getArtistsList);
}
