package com.onregs.audioplayer.ui.interactor.tracks;

import com.onregs.audioplayer.data.Artist;

import java.util.ArrayList;

/**
 * Created by vadim on 14.12.2014.
 */
public interface GetArtistsList
{
    ArrayList<Artist> getArtists();
}
