package com.onregs.audioplayer.ui.dialogs.old_dialogs;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onregs.audioplayer.R;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vadim on 22.12.2014.
 */

@EFragment(R.layout.dialog_add_songs)
public class DialogAddSongs extends DialogFragment
{
    public static final String TAG = DialogAddSongs.class.getSimpleName();

//    public static DialogAddSongs build()
//    {
//        return DialogAddSongs_.builder().build();
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        getDialog().getWindow().setTitle(getResources().getString(R.string.add_songs_title));
        getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_color_dialogtitle));
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
