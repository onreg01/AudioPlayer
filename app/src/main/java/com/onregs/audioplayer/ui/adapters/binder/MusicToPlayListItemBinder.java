package com.onregs.audioplayer.ui.adapters.binder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.onregs.audioplayer.BitmapWorkerTask;
import com.onregs.audioplayer.R;
import com.onregs.audioplayer.data.Track;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vadim on 23.07.2015.
 */
@EViewGroup(R.layout.all_music_multichoice_item)
public class MusicToPlayListItemBinder extends TrackItemViewBinder
{
    @ViewById(R.id.ivTick)
    ImageView tick;

    public MusicToPlayListItemBinder(Context context)
    {
        super(context);
    }

    public MusicToPlayListItemBinder(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public MusicToPlayListItemBinder(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public View bind(Track track, Context context)
    {
        BitmapWorkerTask task = new BitmapWorkerTask(trackImage, context);
        task.execute(track.getPath());

        trackAuthor.setText(track.getArtistInfo().getArtistName());
        trackName.setText(track.getTrackName());
        return this;
    }
}
