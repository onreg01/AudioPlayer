package com.onregs.audioplayer.settings;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * Created by vadim on 01.12.2014.
 */
@EBean(scope = EBean.Scope.Singleton)
public class SettingsFacade
{
    @Pref
    protected Settings_ settings;


    public String getPlayList()
    {
        return settings.playList().get();
    }

    public void setPlayList(String playlist)
    {
        settings.edit().playList().put(playlist).apply();
    }

    public int getTrackPosition()
    {
        return settings.trackPosition().get();
    }

    public void setTrackPosition(int position)
    {

        settings.edit().trackPosition().put(position).apply();
    }

    public void setShuffleState(boolean state)
    {
        settings.edit().isShuffle().put(state).apply();
    }

    public boolean getShuffleState()
    {
        return settings.isShuffle().get();
    }

    public void setRepeatState(boolean state)
    {
        settings.edit().isRepeat().put(state).apply();
    }

    public boolean getRepeatState()
    {
        return settings.isRepeat().get();
    }
}
