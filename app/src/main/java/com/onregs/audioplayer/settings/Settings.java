package com.onregs.audioplayer.settings;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by vadim on 01.12.2014.
 */
@SharedPref(SharedPref.Scope.APPLICATION_DEFAULT)
public interface Settings
{
    @DefaultString("")
    String playList();

    @DefaultInt(0)
    int trackPosition();

    boolean isShuffle();

    boolean isRepeat();
}
