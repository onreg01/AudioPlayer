package com.onregs.audioplayer;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.onregs.audioplayer.utils.ImageUtils;

/**
 * Created by vadim on 03.12.2014.
 */
public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap>
{
    private ImageView imageView;
    private Context context;

    public BitmapWorkerTask(ImageView imageView, Context context)
    {
        this.imageView = imageView;
        this.context = context;
    }

    @Override
    protected Bitmap doInBackground(String... path)
    {
        return ImageUtils.getBitmapImage(path[0], context,0);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap)
    {
        imageView.setImageBitmap(bitmap);
    }
}
