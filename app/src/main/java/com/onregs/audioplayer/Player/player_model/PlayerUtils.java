package com.onregs.audioplayer.Player.player_model;

import android.media.MediaPlayer;

import com.onregs.audioplayer.Player.Player;
import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.data.Track;

/**
 * Created by vadim on 04.12.2014.
 */
public interface PlayerUtils
{
    public void initPlaylist(PlayList playList);

    public MediaPlayer getPlayer();

    public void initPlayerAfterExit();

    public void stopTimer();

    public boolean getState();

    public boolean isShuffle();

    public boolean isRepeat();

    public Track getCurrentTrack();

    public PlayList getCurrentPlaylist();

    public void setPlayList(PlayList playList);

    public int getPosition();

    public void setPosition(int position);

    public boolean isPlayListNoEmpty();

    public void initView(Player.ChangeListViewItemColor changerColor);
}
