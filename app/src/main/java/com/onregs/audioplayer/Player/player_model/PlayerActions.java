package com.onregs.audioplayer.Player.player_model;

/**
 * Created by vadim on 04.12.2014.
 */
public interface PlayerActions
{
    public void start(int position);

    public void nextTrack();

    public void previousTrack();

    public void pause();

    public void unPause();

    public void shuffle(boolean state);

    public void repeat(boolean state);

    public void seekTo(int i);
}
