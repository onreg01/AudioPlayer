package com.onregs.audioplayer.Player;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;

import com.onregs.audioplayer.Player.player_model.PlayerActions;
import com.onregs.audioplayer.Player.player_model.PlayerUtils;
import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.data.Track;
import com.onregs.audioplayer.ui.interactor.broadcast_receiver.ProgressReceiverIml;
import com.onregs.audioplayer.utils.StatePlayer;
import com.onregs.audioplayer.utils.TrackTimer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by vadim on 02.12.2014.
 */

public class Player implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, PlayerActions, PlayerUtils
{
    TrackTimer trackTimer = TrackTimer.getInstance();

    private static Player player;
    private MediaPlayer mediaPlayer;
    private PlayList playList;

    private int trackPosition;
    private ArrayList <Integer> trackPositions;

    private Context context;
    private boolean afterExit;
    private boolean isShuffle;
    private boolean isRepeat;

    public Player(Context context)
    {
        this.context = context;
    }

    public static Player getInstance(Context context)
    {

        if (player == null)
        {
            {
                player = new Player(context);
            }
        }
        return player;
    }


    private void release()
    {
        if (mediaPlayer != null)
        {
            mediaPlayer.release();
        }
    }


    private MediaPlayer initPlayer()
    {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        return mediaPlayer;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer)
    {
        mediaPlayer.start();
        StatePlayer.saveState(context, this);
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer)
    {
        nextTrack();
    }

    @Override
    public void start(int position)
    {
        release();
        trackPosition = position;
        mediaPlayer = initPlayer();
        try
        {
            mediaPlayer.setDataSource(playList.getTrack(position).getPath());
            mediaPlayer.prepare();
            startTimer();
            startReceiver();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        afterExit = false;
        if (changeListViewItemColor != null)
        {
            changeListViewItemColor.switchColor();
        }
    }


    private void nextTrackWithShuffle(int i)
    {
        if(i!=1)
        {
            trackPositions.add(trackPosition);
        }

        Random random = new Random();
        int position = random.nextInt(playList.getSize()-1);
        release();
        start(position);
    }

    @Override
    public void unPause()
    {
        mediaPlayer.start();
        TrackTimer.getInstance().startTimer(context);
    }

    @Override
    public void shuffle(boolean state)
    {
        isShuffle = state;
        if(state)
        {
            trackPositions = new ArrayList<Integer>();
        }
    }

    @Override
    public void repeat(boolean state)
    {
        isRepeat = state;
    }

    @Override
    public void seekTo(int i)
    {
        TrackTimer.getInstance().setTimer(i);
        mediaPlayer.seekTo(i);
    }

    @Override
    public void pause()
    {
        mediaPlayer.pause();
        TrackTimer.getInstance().pauseTimer();
    }

    private void startTimer()
    {
        TrackTimer trackTimer = TrackTimer.getInstance();
        trackTimer.startTimer(context);
    }

    private void startReceiver()
    {
        Intent intent = new Intent(ProgressReceiverIml.BROADCAST_ACTION);
        intent.putExtra(ProgressReceiverIml.STATUS, ProgressReceiverIml.PREPARE_SONG);
        intent.putExtra(ProgressReceiverIml.DATA, playList.getTrack(trackPosition).getDuration().getLongDuration());
        intent.putExtra(ProgressReceiverIml.TRACK_NAME, getCurrentTrack().getTrackName());
        intent.putExtra(ProgressReceiverIml.TRACK_AUTHOR, getCurrentTrack().getArtistInfo().getArtistName());
        context.sendBroadcast(intent);
    }

    @Override
    public void stopTimer()
    {
        trackTimer.stopTimer();
    }

    @Override
    public void nextTrack()
    {
        stopTimer();
        if(isRepeat())
        {
            start(trackPosition);
            return;
        }
        if(isShuffle)
        {
            nextTrackWithShuffle(0);
            return;
        }
        if (checkFinishList())
        {
            trackPosition = 0;
        }

        else
        {
            trackPosition++;
        }
        release();
        start(trackPosition);
    }

    @Override
    public void previousTrack()
    {
        stopTimer();
        if(isShuffle)
        {
          previousTrackWithShuffle();
          return;
        }
        if (trackPosition == 0)
        {
            trackPosition = playList.getSize() - 1;
        }
        else
        {
            trackPosition--;
        }
        release();
        start(trackPosition);
    }

    private void previousTrackWithShuffle()
    {
        if(trackPositions.isEmpty())
        {
            nextTrackWithShuffle(1);
            return;
        }
        int position = trackPositions.get(trackPositions.size()-1);
        trackPositions.remove(trackPositions.size()-1);
        start(position);
    }

    public boolean checkFinishList()
    {
        return (trackPosition == playList.getSize() - 1);
    }

    @Override
    public void initPlayerAfterExit()
    {
        StatePlayer.restoreState(context, this);
        afterExit = true;
    }

    @Override
    public Track getCurrentTrack()
    {
        return playList.getTrack(trackPosition);
    }

    @Override
    public MediaPlayer getPlayer()
    {
        return mediaPlayer;
    }

    @Override
    public PlayList getCurrentPlaylist()
    {
        return playList;
    }

    @Override
    public int getPosition()
    {
        return trackPosition;
    }

    @Override
    public void setPosition(int position)
    {
        trackPosition = position;
    }

    @Override
    public void setPlayList(PlayList playList)
    {
        this.playList = playList;
    }

    @Override
    public void initPlaylist(PlayList playList)
    {
        this.playList = playList;
    }

    @Override
    public boolean getState()
    {
        return afterExit;
    }

    @Override
    public boolean isShuffle()
    {
        return isShuffle;
    }

    @Override
    public boolean isRepeat()
    {
        return isRepeat;
    }

    @Override
    public boolean isPlayListNoEmpty()
    {
        return playList != null;
    }

    public ChangeListViewItemColor changeListViewItemColor;

    public interface ChangeListViewItemColor
    {
        void switchColor();
    }

    @Override
    public void initView(ChangeListViewItemColor changerColor)
    {
        changeListViewItemColor = changerColor;
    }
}
