package com.onregs.audioplayer.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by vadim on 29.11.2014.
 */
public class DateUtils
{
    private static final String FORMAT = "m:ss";

    public static String getStringDuration(Long duration)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT, Locale.getDefault());
        return sdf.format(new Date(duration));
    }
}
