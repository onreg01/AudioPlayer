package com.onregs.audioplayer.utils;

import android.content.Context;
import android.content.Intent;


import com.onregs.audioplayer.ui.interactor.broadcast_receiver.ProgressReceiverIml;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by vadim on 04.12.2014.
 */
public class TrackTimer
{

    private static TrackTimer trackTimer;

    public static TrackTimer getInstance()
    {
        if (trackTimer == null)
        {
            trackTimer = new TrackTimer();
        }
        return trackTimer;
    }


    private SimpleDateFormat simpleDateFormat;
    private Calendar calendar;
    private GameTimerTask gameTimerTask;
    private long time = 0;
    private Timer timer;

    public void startTimer(Context context)
    {
        gameTimerTask = new GameTimerTask(context);
        timer = new Timer();
        timer.schedule(gameTimerTask, 0, 1000);
    }

    public void stopTimer()
    {
        if (timer != null)
        {
            cancelTimer();
            time = 0;
        }
    }

    public void pauseTimer()
    {
        cancelTimer();
    }

    private void cancelTimer()
    {
        timer.cancel();
    }

    public long getTimer()
    {
        pauseTimer();
        return time;
    }

    public void setTimer(long time)
    {
        this.time = time;
    }

    class GameTimerTask extends TimerTask
    {
        Context context;

        GameTimerTask(Context context)
        {
            this.context = context;
        }

        @Override
        public void run()
        {
            calendar = Calendar.getInstance();
            time += 1000;
            calendar.setTimeInMillis(time);
            Intent intent = new Intent(ProgressReceiverIml.BROADCAST_ACTION);
            intent.putExtra(ProgressReceiverIml.STATUS, ProgressReceiverIml.UPDATE_PROGRESS);
            intent.putExtra(ProgressReceiverIml.DATA, calendar.getTimeInMillis());
            context.sendBroadcast(intent);

        }
    }


}
