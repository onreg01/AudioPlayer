package com.onregs.audioplayer.utils;

import android.content.Context;

import com.onregs.audioplayer.Player.player_model.PlayerActions;
import com.onregs.audioplayer.Player.player_model.PlayerUtils;
import com.onregs.audioplayer.data.PlayList;
import com.onregs.audioplayer.settings.SettingsFacade;
import com.onregs.audioplayer.settings.SettingsFacade_;

/**
 * Created by vadim on 07.12.2014.
 */
public class StatePlayer
{

    public static void saveState(Context context, PlayerUtils playerUtils)
    {
        SettingsFacade settings = SettingsFacade_.getInstance_(context);

        PlayList playList = playerUtils.getCurrentPlaylist();

        settings.setPlayList(GsonUtils.convertPlaylist(playList));
        settings.setTrackPosition(playerUtils.getPosition());
    }


    public static void restoreState(Context context, PlayerUtils playerUtils)
    {
        SettingsFacade settings = SettingsFacade_.getInstance_(context);

        PlayList playList = GsonUtils.restorePlaylist(settings.getPlayList());

        playerUtils.setPlayList(playList);
        playerUtils.setPosition(settings.getTrackPosition());
    }

    public static void saveOptionButtons(Context context, PlayerUtils playerUtils)
    {
        SettingsFacade settings = SettingsFacade_.getInstance_(context);
        settings.setShuffleState(playerUtils.isShuffle());
        settings.setRepeatState(playerUtils.isRepeat());
    }

    public static void restoreOptionButtons(Context context, PlayerActions playerActions)
    {
        SettingsFacade settings = SettingsFacade_.getInstance_(context);
        playerActions.shuffle(settings.getShuffleState());
        playerActions.repeat(settings.getRepeatState());
    }
}
