package com.onregs.audioplayer.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;

import java.io.IOException;

/**
 * Created by vadim on 29.11.2014.
 */
public class ImageUtils
{
    public static final String DEFAULT_IMAGE = "default_image.png";
    public static final String IMAGE_FOR_LIST = "image_for_list.png";

    public static Bitmap getBitmapImage(String path, Context context, int typeImage)
    {
        Bitmap albumImage = null;

        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(path);

        byte[] art = metaRetriever.getEmbeddedPicture();

        if (art != null)
        {
            albumImage = BitmapFactory.decodeByteArray(art, 0, art.length);
        }

        else
        {
            try
            {
                if(typeImage == 0)
                {
                    albumImage = BitmapFactory.decodeStream(context.getAssets().open(IMAGE_FOR_LIST));
                }
                else
                {
                    albumImage = BitmapFactory.decodeStream(context.getAssets().open(DEFAULT_IMAGE));
                }

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return albumImage;
    }

}

