package com.onregs.audioplayer.utils;

import com.google.gson.Gson;
import com.onregs.audioplayer.data.PlayList;

/**
 * Created by vadim on 01.12.2014.
 */
public class GsonUtils
{
    private static final Gson gson = new Gson();

    public static String convertPlaylist(PlayList playList)
    {
        return gson.toJson(playList);
    }

    public static PlayList restorePlaylist(String playlist)
    {
        return gson.fromJson(playlist, PlayList.class);
    }
}
