package com.onregs.audioplayer.utils;

import android.content.Context;

import com.onregs.audioplayer.ui.activity.AlbumsListActivity_;
import com.onregs.audioplayer.ui.activity.AllMusicListActivity_;
import com.onregs.audioplayer.ui.activity.ArtistsListActivity_;
import com.onregs.audioplayer.ui.activity.FaceTrackActivity_;
import com.onregs.audioplayer.ui.activity.MusicToPlayList_;
import com.onregs.audioplayer.ui.activity.PlayListsActivity_;

/**
 * Created by vadim on 20.11.2014.
 */
public class Luncher
{

    public static void startAllMusicListActivity(Context context)
    {
        AllMusicListActivity_.intent(context).start();
    }

    public static void startAlbumsListActivity(Context context)
    {
        AlbumsListActivity_.intent(context).start();
    }

    public static void startArtistsListActivity(Context context)
    {
        ArtistsListActivity_.intent(context).start();
    }

    public static void startPlayListsActivity(Context context)
    {
        PlayListsActivity_.intent(context).start();
    }

    public static void startMusicToPlayListActivity(Context context, String playListName)
    {
        MusicToPlayList_.intent(context).playListName(playListName).startForResult(1);
    }

    public static void startFaceTrackActivity(Context context)
    {
        FaceTrackActivity_.intent(context).start();
    }

}
