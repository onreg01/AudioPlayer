package com.onregs.audioplayer.utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.onregs.audioplayer.data.Track;

import java.util.ArrayList;

/**
 * Created by vadim on 19.12.2014.
 */
public class PlayListUtils
{

    public static Cursor getAllPlayLists(ContentResolver resolver)
    {
        return resolver.query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, null, null, null, null);
    }

    public static long getPlaylist(ContentResolver resolver, String name)
    {
        long id = -1;

        Cursor cursor = resolver.query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Playlists._ID},
                MediaStore.Audio.Playlists.NAME + "=?",
                new String[]{name}, null);

        if (cursor != null)
        {
            if (cursor.moveToNext())
            {
                id = cursor.getLong(0);
            }
            cursor.close();
        }

        return id;
    }

    public static long createPlaylist(ContentResolver resolver, String name)
    {
        long id = getPlaylist(resolver, name);

        if (id == -1)
        {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Audio.Playlists.NAME, name);
            Uri uri = resolver.insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, values);
            id = Long.parseLong(uri.getLastPathSegment());
        }
        else
        {
            //**clear playlist*/
//            Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", id);
//            resolver.delete(uri, null, null);
            return 0;
        }

        return id;
    }

    /**
     * Run the given query and add the results to the given playlist. Should be
     * run on a background thread.
     *
     * @param resolver   A ContentResolver to use.
     * @param playlistId The MediaStore.Audio.Playlist id of the playlist to
     *                   modify.
     * @return The number of songs that were added to the playlist.
     */
    public static int addToPlaylist(ContentResolver resolver, long playlistId, ArrayList<Track> tracks)
    {
        if (playlistId == -1)
        {
            return 0;
        }

        // Find the greatest PLAY_ORDER in the playlist
        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
        String[] projection = new String[]{MediaStore.Audio.Playlists.Members.PLAY_ORDER};
        Cursor cursor = resolver.query(uri, projection, null, null, null);
        int base = 0;
        if (cursor.moveToLast())
        {
            base = cursor.getInt(0) + 1;
        }
        cursor.close();


        int count = tracks.size();
        if (count > 0)
        {
            ContentValues[] values = new ContentValues[count];
            for (int i = 0; i != count; ++i)
            {
                ContentValues value = new ContentValues(2);
                value.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, Integer.valueOf(base + i));
                value.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, tracks.get(i).getTrackId());
                values[i] = value;
            }
            resolver.bulkInsert(uri, values);
        }
        return count;
    }


//    public static void deletePlaylist(ContentResolver resolver, long id)
//    {
//        Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, id);
//        resolver.delete(uri, null, null);
//    }

    public static void deletePlaylist(ContentResolver resolver, long id)
    {

        resolver.delete(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                MediaStore.Audio.Playlists._ID + "=?",
                new String[]{"" + id}
        );
    }


    /**
     * @return 0 playlist name is already.
     * return 1 playlist renamed
     */
    public static long renamePlaylist(ContentResolver resolver, long id, String newName)
    {
        long existingId = getPlaylist(resolver, newName);
        if (existingId == id || existingId != -1)
        {
            return 0;
        }

        ContentValues values = new ContentValues(1);
        values.put(MediaStore.Audio.Playlists.NAME, newName);
        resolver.update(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, values, "_id=" + id, null);
        return 1;
    }
}
