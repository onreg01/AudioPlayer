package com.onregs.audioplayer.data;

import java.util.ArrayList;

/**
 * Created by vadim on 04.12.2014.
 */
public class PlayList
{
    private long playListId;
    private String playlistName;
    private ArrayList<Track> tracks;
    private int tracksCount;

    public PlayList(String playlistName)
    {
        this.playlistName = playlistName;
        tracks = new ArrayList<Track>();
        this.tracksCount = 0;
    }

    public String getPlaylistName()
    {
        return playlistName;
    }

    public void setPlaylistName(String playlistName)
    {
        this.playlistName = playlistName;
    }

    public ArrayList<Track> getTracks()
    {
        return tracks;
    }

    public void addTrack(Track track)
    {
        track.setTrackPosition(tracksCount);
        tracks.add(track);
        tracksCount++;
    }

    public void setTracks(ArrayList<Track> tracks)
    {
        for(Track track:tracks)
        {
            addTrack(track);
        }
    }

    public int getSize()
    {
        return tracksCount;
    }

    public long getPlayListId()
    {
        return playListId;
    }

    public void setPlayListId(long playListId)
    {
        this.playListId = playListId;
    }

    public Track getTrack(int position)
    {
        return tracks.get(position);
    }

}
