package com.onregs.audioplayer.data;

import com.onregs.audioplayer.data.track_info.ArtistInfo;

/**
 * Created by vadim on 08.12.2014.
 */
public class Album
{
    private String albumName;
    private int albumId;
    private ArtistInfo artistInfo;
    private PlayList playList;

    public Album(String albumName, int albumId)
    {
        this.albumName = albumName;
        this.albumId = albumId;
        playList = new PlayList(albumName);
    }

    public String getAlbumName()
    {
        return albumName;
    }

    public void setAlbumName(String albumName)
    {
        this.albumName = albumName;
    }

    public PlayList getPlayList()
    {
        return playList;
    }

    public void setPlayList(PlayList playList)
    {
        this.playList = playList;
    }

    public ArtistInfo getArtistInfo()
    {
        return artistInfo;
    }

    public void setArtistInfo(ArtistInfo artistInfo)
    {
        this.artistInfo = artistInfo;
    }

    @Override
    public boolean equals(Object object)
    {
        boolean isEqual = false;

        if (object != null && object instanceof Album)
        {
            isEqual = (this.albumId == ((Album) object).albumId);
        }

        return isEqual;
    }
}
