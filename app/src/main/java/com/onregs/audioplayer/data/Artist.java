package com.onregs.audioplayer.data;

import java.util.ArrayList;

/**
 * Created by vadim on 08.12.2014.
 */
public class Artist
{
    private int artistId;
    private String artistName;
    private ArrayList<Album> albums = new ArrayList<Album>();


    public void setArtistId(int artistId)
    {
        this.artistId = artistId;
    }

    public int getArtistId()
    {
        return artistId;
    }

    public void setArtistName(String artistName)
    {
        this.artistName = artistName;
    }


    public String getArtistName()
    {
        return artistName;
    }


    public ArrayList<Album> getAlbums()
    {
        return albums;
    }

    public void setAlbums(ArrayList<Album> albums)
    {
        this.albums = albums;
    }

    public void addAlbum(Album album)
    {
        albums.add(album);
    }

    public int getAlbumsCount()
    {
        return albums.size();
    }

    @Override
    public boolean equals(Object object)
    {
        boolean isEqual = false;

        if (object != null && object instanceof Artist)
        {
            isEqual = (this.artistId == ((Artist) object).artistId);
        }

        return isEqual;
    }
}
