package com.onregs.audioplayer.data;

import com.onregs.audioplayer.utils.DateUtils;
/**
 * Created by vadim on 30.11.2014.
 */
public class TrackDuration
{

    private String stringDuration;
    private long duration;

    public TrackDuration(long duration)
    {
        this.duration = duration;
        this.stringDuration = DateUtils.getStringDuration(duration);
    }

    public long getLongDuration()
    {
        return duration;
    }

    public void setDuration(long duration)
    {
        this.duration = duration;
        this.stringDuration = DateUtils.getStringDuration(duration);
    }

    public String getStringDuration()
    {
        return stringDuration;
    }
}
