package com.onregs.audioplayer.data;

import com.onregs.audioplayer.data.track_info.AlbumInfo;
import com.onregs.audioplayer.data.track_info.ArtistInfo;

/**
 * Created by vadim on 29.11.2014.
 */


public class Track
{
    private int trackId;
    private String trackName;
    private String path;

    private TrackDuration duration;
    private AlbumInfo albumInfo;
    private ArtistInfo artistInfo;

    private int trackPosition;

    public Track(int trackId, String trackName, String path, TrackDuration duration, AlbumInfo albumInfo, ArtistInfo artistInfo)
    {
        this.trackId = trackId;
        this.trackName = trackName;
        this.path = path;
        this.duration = duration;

        this.albumInfo = albumInfo;
        this.artistInfo = artistInfo;

    }

    public TrackDuration getDuration()
    {
        return duration;
    }

    public void setDuration(TrackDuration duration)
    {
        this.duration = duration;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public String getTrackName()
    {
        return trackName;
    }

    public void setTrackName(String trackName)
    {
        this.trackName = trackName;
    }

    public int getTrackPosition()
    {
        return trackPosition;
    }

    public void setTrackPosition(int trackPosition)
    {
        this.trackPosition = trackPosition;
    }

    public int getTrackId()
    {
        return trackId;
    }

    public void setTrackId(int trackId)
    {
        this.trackId = trackId;
    }

    public AlbumInfo getAlbumInfo()
    {
        return albumInfo;
    }

    public void setAlbumInfo(AlbumInfo albumInfo)
    {
        this.albumInfo = albumInfo;
    }

    public ArtistInfo getArtistInfo()
    {
        return artistInfo;
    }

    public void setArtistInfo(ArtistInfo artistInfo)
    {
        this.artistInfo = artistInfo;
    }
}
