package com.onregs.audioplayer.data.track_info;

/**
 * Created by vadim on 13.12.2014.
 */
public class AlbumInfo
{
    private int albumId;
    private String albumName;

    public AlbumInfo(int albumId, String albumName)
    {
        this.albumId = albumId;
        this.albumName = albumName;
    }

    public int getAlbumId()
    {
        return albumId;
    }

    public void setAlbumId(int albumId)
    {
        this.albumId = albumId;
    }

    public String getAlbumName()
    {
        return albumName;
    }

    public void setAlbumName(String albumName)
    {
        this.albumName = albumName;
    }
}
