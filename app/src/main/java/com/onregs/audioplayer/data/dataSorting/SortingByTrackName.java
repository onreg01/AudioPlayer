package com.onregs.audioplayer.data.dataSorting;

import com.onregs.audioplayer.data.Track;

import java.util.Comparator;

/**
 * Created by vadim on 03.12.2014.
 */
public class SortingByTrackName implements Comparator<Track>
{
    @Override
    public int compare(Track firstTrack, Track secondTrack)
    {
        String nameFirst = firstTrack.getTrackName();
        String nameSecond = secondTrack.getTrackName();

        return nameFirst.compareTo(nameSecond);
    }
}
