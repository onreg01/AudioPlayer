package com.onregs.audioplayer.data.track_info;

/**
 * Created by vadim on 13.12.2014.
 */
public class ArtistInfo
{
    private int artistId;
    private String artistName;

    public ArtistInfo(int artistId, String artistName)
    {
        this.artistId = artistId;
        this.artistName = artistName;
    }

    public int getArtistId()
    {
        return artistId;
    }

    public void setArtistId(int artistId)
    {
        this.artistId = artistId;
    }

    public String getArtistName()
    {
        return artistName;
    }

    public void setArtistName(String artistName)
    {
        this.artistName = artistName;
    }
}
